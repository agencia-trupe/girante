<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Projeto;
use App\Models\Marcenaria;
use Illuminate\Http\Request;
use App\Models\MarcenariaImagem;
use App\Http\Controllers\Controller;

class MarcenariaController extends Controller
{
    public function index(Marcenaria $categoria_selecionada)
    {
        if (!$categoria_selecionada->id) {
            $imagens = MarcenariaImagem::orderByRaw('RAND()')->take(12)->get();
        } else {
            $imagens = $categoria_selecionada->imagens;
        }

        $categorias_select = Marcenaria::ordenados()->lists('titulo', 'slug');
        $categorias = Marcenaria::ordenados()->get();
        $projetos   = Projeto::with('imagens')->ordenados()->get();


        return view('frontend.marcenaria', compact('categorias', 'categorias_select', 'categoria_selecionada', 'imagens', 'projetos'));
    }
}
