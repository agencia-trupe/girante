<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Requests;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use App\Models\BlogCategoria;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogComentariosRequest;

class BlogController extends Controller
{

    public function index(BlogCategoria $categoria, Request $request)
    {
        $posts = BlogPost::with('categoria')->ordenados();
        if (!empty($categoria->id)) $posts = $posts->categoria($categoria->id);

        $mes_selecionado = null;
        $ano_selecionado = null;

        if ($request->ajax()) return $posts->paginate(10);

        return view('frontend.blog.index', compact('categoria', 'mes_selecionado', 'ano_selecionado'));
    }

    public function show(BlogCategoria $categoria, BlogPost $post)
    {
        $mes_selecionado = null;
        $ano_selecionado = null;

        $data_post = Carbon::createFromFormat('d/m/Y', $post->data)->format('Y-m-d');

        $anterior = BlogPost::where('data', '<', $data_post)
                          ->orWhere(function($query) use ($data_post, $post) {
                              $query->where('data', $data_post)
                                    ->where('id', '<', $post->id);
                          })
                          ->orderBy('data', 'DESC')->orderBy('id', 'DESC')->first();

        $proximo  = BlogPost::where('data', '>', $data_post)
                          ->orWhere(function($query) use ($data_post, $post) {
                              $query->where('data', $data_post)
                                    ->where('id', '>', $post->id);
                          })->orderBy('data', 'ASC')->orderBy('id', 'ASC')->first();

        return view('frontend.blog.show', compact('post', 'anterior', 'proximo', 'mes_selecionado', 'ano_selecionado', 'categoria'));
    }

    public function data(Request $request, $ano_selecionado = null, $mes_selecionado = null)
    {
        $posts = BlogPost::with('comentariosAprovados', 'categoria')->ordenados();

        if (!$mes_selecionado) {
            $posts = $posts->whereRaw("YEAR(data) = $ano_selecionado");
        } else {
            $posts = $posts->whereRaw("YEAR(data) = $ano_selecionado AND MONTH(data) = $mes_selecionado");
        }

        if ($request->ajax()) return $posts->paginate(10);

        return view('frontend.blog.index', compact('ano_selecionado', 'mes_selecionado'));
    }

    public function comentario(BlogComentariosRequest $request, BlogCategoria $categoria, BlogPost $post)
    {
        $post->comentarios()->create($request->all());

        return response()->json(['message' => 'Comentário enviado com sucesso! Aguardando aprovação.']);
    }

}
