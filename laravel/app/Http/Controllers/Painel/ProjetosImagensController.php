<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProjetosImagensRequest;

use App\Http\Controllers\Controller;
use App\Models\Projeto;
use App\Models\ProjetoImagem;

use App\Helpers\CropImage;

class ProjetosImagensController extends Controller
{
    private $image_config = [
        [
            'width'     => 180,
            'height'    => 180,
            'greyscale' => true,
            'path'      => 'assets/img/projetos/imagens/thumbs/'
        ],
        [
            'width'     => 980,
            'height'    => null,
            'upsize'    => true,
            'path'      => 'assets/img/projetos/imagens/'
        ]
    ];

    public function index(Projeto $projeto)
    {
        $imagens = ProjetoImagem::projeto($projeto->id)->ordenados()->get();

        return view('painel.projetos.imagens.index', compact('imagens', 'projeto'));
    }

    public function show(Projeto $projeto, ProjetoImagem $imagem)
    {
        return $imagem;
    }

    public function create(Projeto $projeto)
    {
        return view('painel.projetos.imagens.create', compact('projeto'));
    }

    public function store(Projeto $projeto, ProjetosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['projeto_id'] = $projeto->id;

            $imagem = ProjetoImagem::create($input);
            $view = view('painel.projetos.imagens.imagem', compact('projeto', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function update(ProjetosImagensRequest $request, Projeto $projeto, ProjetoImagem $imagem)
    {
        try {

            $imagem->update(['legenda' => $request->get('legenda')]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Projeto $projeto, ProjetoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.marcenaria-personalizada.projetos.imagens.index', $projeto)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
