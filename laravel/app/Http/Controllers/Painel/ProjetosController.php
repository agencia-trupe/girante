<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Helpers\CropImage;

class ProjetosController extends Controller
{
    private $image_config = [
        'width'  => 245,
        'height' => 245,
        'path'   => 'assets/img/projetos/'
    ];

    public function index()
    {
        $projetos = Projeto::ordenados()->get();

        return view('painel.projetos.index', compact('projetos'));
    }

    public function create()
    {
        return view('painel.projetos.create');
    }

    public function store(ProjetosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Projeto::create($input);
            return redirect()->route('painel.marcenaria-personalizada.projetos.index')->with('success', 'Projeto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar projeto: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $projeto)
    {
        return view('painel.projetos.edit', compact('projeto'));
    }

    public function update(ProjetosRequest $request, Projeto $projeto)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $projeto->update($input);
            return redirect()->route('painel.marcenaria-personalizada.projetos.index')->with('success', 'Projeto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar projeto: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $projeto)
    {
        try {

            $projeto->delete();
            return redirect()->route('painel.marcenaria-personalizada.projetos.index')->with('success', 'Projeto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir projeto: '.$e->getMessage()]);

        }
    }
}
