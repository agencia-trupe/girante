<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosRequest;
use App\Models\ProdutoCategoria;
use App\Models\Linha;
use App\Models\Produto;
use Illuminate\Http\Request;

class ProdutosController extends Controller
{
    private $categorias;
    private $linhas;

    public function __construct()
    {
        $this->categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');
        $this->linhas     = Linha::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $linhas     = $this->linhas;
        $filtro     = $request->query('filtro');

        if (ProdutoCategoria::find($filtro)) {
            $produtos = Produto::ordenados()->categoria($filtro)->get();
        } else {
            $produtos = Produto::join('produtos_categorias as cat', 'cat.id', '=', 'produtos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->get();
        }

        return view('painel.produtos.index', compact('categorias', 'linhas', 'produtos', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;
        $linhas     = $this->linhas;

        return view('painel.produtos.create', compact('categorias', 'linhas'));
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();

            Produto::create($input);
            return redirect()->route('painel.linha-girante.produtos.index')->with('success', 'Produto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar produto: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto)
    {
        $categorias = $this->categorias;
        $linhas     = $this->linhas;

        return view('painel.produtos.edit', compact('categorias', 'linhas', 'produto'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            $produto->update($input);
            return redirect()->route('painel.linha-girante.produtos.index')->with('success', 'Produto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar produto: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto)
    {
        try {

            $produto->delete();
            return redirect()->route('painel.linha-girante.produtos.index')->with('success', 'Produto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir produto: '.$e->getMessage()]);

        }
    }
}
