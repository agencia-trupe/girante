<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\CropImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogPostsRequest;
use App\Models\BlogCategoria;
use App\Models\BlogPost;
use Illuminate\Http\Request;

class BlogPostsController extends Controller
{
    private $image_config = [
        'width'  => 380,
        'height' => null,
        'path'   => 'assets/img/blog/'
    ];

    private $categorias;

    public function __construct()
    {
        $this->categorias = BlogCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (BlogCategoria::find($filtro)) {
            $posts = BlogPost::ordenados()->categoria($filtro)->paginate(20);
        } else {
            $posts = BlogPost::ordenados()->paginate(20);
        }

        return view('painel.blog.index', compact('categorias', 'posts', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.blog.create', compact('categorias'));
    }

    public function store(BlogPostsRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            BlogPost::create($input);
            return redirect()->route('painel.novidades.index')->with('success', 'Post adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar post: '.$e->getMessage()]);

        }
    }

    public function edit(BlogPost $post)
    {
        $categorias = $this->categorias;

        return view('painel.blog.edit', compact('categorias', 'post'));
    }

    public function update(BlogPostsRequest $request, BlogPost $post)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $post->update($input);
            return redirect()->route('painel.novidades.index')->with('success', 'Post alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar post: '.$e->getMessage()]);

        }
    }

    public function destroy(BlogPost $post)
    {
        try {

            $post->delete();
            return redirect()->route('painel.novidades.index')->with('success', 'Post excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir post: '.$e->getMessage()]);

        }
    }

    public function imageUpload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'imagem' => 'image|required'
        ]);

        if ($validator->fails()) {
            $response = [
                'error'   => true,
                'message' => 'O arquivo deve ser uma imagem.'
            ];
        } else {
            $imagem   = CropImage::make('imagem', [
                'width'  => 800,
                'height' => null,
                'path'   => 'assets/img/blog/imagens/'
            ]);
            $response = [
                'filepath' => asset('assets/img/blog/imagens/' . $imagem)
            ];
        }

        return response()->json($response);
    }
}
