<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogCategoriasRequest;
use App\Models\BlogCategoria;
use Illuminate\Http\Request;

class BlogCategoriasController extends Controller
{
    public function index()
    {
        $categorias = BlogCategoria::ordenados()->get();

        return view('painel.blog.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.blog.categorias.create');
    }

    public function store(BlogCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            BlogCategoria::create($input);
            return redirect()->route('painel.novidades.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(BlogCategoria $categoria)
    {
        return view('painel.blog.categorias.edit', compact('categoria'));
    }

    public function update(BlogCategoriasRequest $request, BlogCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.novidades.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(BlogCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.novidades.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
