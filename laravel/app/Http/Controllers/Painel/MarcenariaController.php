<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MarcenariaRequest;
use App\Http\Controllers\Controller;

use App\Models\Marcenaria;
use App\Helpers\CropImage;

class MarcenariaController extends Controller
{
    public function index()
    {
        $marcenaria = Marcenaria::ordenados()->get();

        return view('painel.marcenaria.categorias.index', compact('marcenaria'));
    }

    public function create()
    {
        return view('painel.marcenaria.categorias.create');
    }

    public function store(MarcenariaRequest $request)
    {
        try {

            $input = $request->all();

            Marcenaria::create($input);
            return redirect()->route('painel.marcenaria-personalizada.imagens.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(Marcenaria $marcenaria)
    {
        return view('painel.marcenaria.categorias.edit', compact('marcenaria'));
    }

    public function update(MarcenariaRequest $request, Marcenaria $marcenaria)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            $marcenaria->update($input);
            return redirect()->route('painel.marcenaria-personalizada.imagens.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(Marcenaria $marcenaria)
    {
        try {

            $marcenaria->delete();
            return redirect()->route('painel.marcenaria-personalizada.imagens.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
