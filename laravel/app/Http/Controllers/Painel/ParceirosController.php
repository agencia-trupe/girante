<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ParceiroRequest;
use App\Http\Controllers\Controller;

use App\Models\Parceiro;
use App\Helpers\CropImage;

class ParceirosController extends Controller
{
    private $image_config = [
        'width'  => 220,
        'height' => 220,
        'path'   => 'assets/img/quem-somos/parceiros/'
    ];

    public function index()
    {
        $parceiros = Parceiro::ordenados()->get();

        return view('painel.quem-somos.parceiros.index', compact('parceiros'));
    }

    public function create()
    {
        return view('painel.quem-somos.parceiros.create');
    }

    public function store(ParceiroRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Parceiro::create($input);
            return redirect()->route('painel.quem-somos.parceiros.index')->with('success', 'Parceiro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar parceiro: '.$e->getMessage()]);

        }
    }

    public function edit(Parceiro $parceiro)
    {
        return view('painel.quem-somos.parceiros.edit', compact('parceiro'));
    }

    public function update(ParceiroRequest $request, Parceiro $parceiro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $parceiro->update($input);
            return redirect()->route('painel.quem-somos.parceiros.index')->with('success', 'Parceiro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar parceiro: '.$e->getMessage()]);

        }
    }

    public function destroy(Parceiro $parceiro)
    {
        try {

            $parceiro->delete();
            return redirect()->route('painel.quem-somos.parceiros.index')->with('success', 'Parceiro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir parceiro: '.$e->getMessage()]);

        }
    }
}
