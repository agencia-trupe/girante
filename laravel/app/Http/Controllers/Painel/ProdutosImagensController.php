<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProdutosImagensRequest;

use App\Http\Controllers\Controller;
use App\Models\Produto;
use App\Models\ProdutoImagem;

use App\Helpers\CropImage;

class ProdutosImagensController extends Controller
{
    private $image_config = [
        [
            'width'     => 180,
            'height'    => 180,
            'greyscale' => true,
            'path'      => 'assets/img/produtos/imagens/thumbs/'
        ],
        [
            'width'     => 560,
            'height'    => null,
            'path'      => 'assets/img/produtos/imagens/'
        ]
    ];

    public function index(Produto $produto)
    {
        $imagens = ProdutoImagem::produto($produto->id)->ordenados()->get();

        return view('painel.produtos.imagens.index', compact('imagens', 'produto'));
    }

    public function show(Produto $produto, ProdutoImagem $imagem)
    {
        return $imagem;
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.imagens.create', compact('produto'));
    }

    public function store(Produto $produto, ProdutosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['produto_id'] = $produto->id;

            $imagem = ProdutoImagem::create($input);
            $view = view('painel.produtos.imagens.imagem', compact('produto', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Produto $produto, ProdutoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.linha-girante.produtos.imagens.index', $produto)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
