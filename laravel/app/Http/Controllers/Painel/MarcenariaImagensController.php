<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\MarcenariaImagensRequest;

use App\Http\Controllers\Controller;
use App\Models\Marcenaria;
use App\Models\MarcenariaImagem;

use App\Helpers\CropImage;

class MarcenariaImagensController extends Controller
{
    private $image_config = [
        [
            'width'     => 180,
            'height'    => 180,
            'greyscale' => true,
            'path'      => 'assets/img/marcenaria/imagens/thumbs/'
        ],
        [
            'width'     => 980,
            'height'    => null,
            'upsize'    => true,
            'path'      => 'assets/img/marcenaria/imagens/'
        ]
    ];

    public function index()
    {
        $imagens = MarcenariaImagem::ordenados()->get();

        return view('painel.marcenaria.index', compact('imagens'));
    }

    public function create()
    {
        $categorias = Marcenaria::ordenados()->lists('titulo', 'id');

        return view('painel.marcenaria.create', compact('categorias'));
    }

    public function store(MarcenariaImagensRequest $request)
    {
        try {

            $input = $request->except('categorias_list');

            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = MarcenariaImagem::create($input);
            $imagem->categorias()->sync($request->input('categorias_list'));

            return redirect()->route('painel.marcenaria-personalizada.imagens.index')->with('success', 'Imagem adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar imagem: '.$e->getMessage()]);

        }
    }

    public function edit(MarcenariaImagem $imagem)
    {
        $categorias = Marcenaria::ordenados()->lists('titulo', 'id');

        return view('painel.marcenaria.edit', compact('categorias', 'imagem'));
    }

    public function update(MarcenariaImagensRequest $request, MarcenariaImagem $imagem)
    {
        try {


            $input = array_filter($request->except('categorias_list'), 'strlen');

            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $imagem->update($input);
            $imagem->categorias()->sync($request->input('categorias_list'));

            return redirect()->route('painel.marcenaria-personalizada.imagens.index')->with('success', 'Imagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar imagem: '.$e->getMessage()]);

        }
    }

    public function destroy(MarcenariaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.marcenaria-personalizada.imagens.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
