<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\BlogComentario;
use Illuminate\Http\Request;

class BlogComentariosController extends Controller
{
    public function index()
    {
        $comentarios = BlogComentario::with('post')->ordenados()->paginate(20);

        return view('painel.blog.comentarios', compact('comentarios'));
    }

    public function destroy(BlogComentario $comentario)
    {
        try {

            $comentario->delete();
            return redirect()->route('painel.novidades.comentarios.index')->with('success', 'Comentário excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir comentário: '.$e->getMessage()]);

        }
    }

    public function aprovacao(BlogComentario $comentario, $value)
    {
        $comentario->aprovado = $value;
        $comentario->save();
    }
}
