<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinhasRequest;
use App\Http\Controllers\Controller;

use App\Models\Linha;
use App\Helpers\CropImage;

class LinhasController extends Controller
{
    public function index()
    {
        $linhas = Linha::ordenados()->get();

        return view('painel.linhas.index', compact('linhas'));
    }

    public function create()
    {
        return view('painel.linhas.create');
    }

    public function store(LinhasRequest $request)
    {
        try {

            $input = $request->all();

            Linha::create($input);
            return redirect()->route('painel.linha-girante.linhas.index')->with('success', 'Linha adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar linha: '.$e->getMessage()]);

        }
    }

    public function edit(Linha $linha)
    {
        return view('painel.linhas.edit', compact('linha'));
    }

    public function update(LinhasRequest $request, Linha $linha)
    {
        try {

            $input = $request->all();

            $linha->update($input);
            return redirect()->route('painel.linha-girante.linhas.index')->with('success', 'Linha alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar linha: '.$e->getMessage()]);

        }
    }

    public function destroy(Linha $linha)
    {
        try {

            $linha->delete();
            return redirect()->route('painel.linha-girante.linhas.index')->with('success', 'Linha excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir linha: '.$e->getMessage()]);

        }
    }
}
