<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\LinhasImagensRequest;

use App\Http\Controllers\Controller;
use App\Models\Linha;
use App\Models\LinhaImagem;

use App\Helpers\CropImage;

class LinhasImagensController extends Controller
{
    private $image_config = [
        [
            'width'     => 180,
            'height'    => 180,
            'greyscale' => true,
            'path'      => 'assets/img/linhas/imagens/thumbs/'
        ],
        [
            'width'     => 180,
            'height'    => 180,
            'greyscale' => true,
            'path'      => 'assets/img/linhas/thumbs/'
        ],
        [
            'width'     => 980,
            'height'    => null,
            'upsize'    => true,
            'path'      => 'assets/img/linhas/imagens/'
        ]
    ];

    public function index(Linha $linha)
    {
        $imagens = LinhaImagem::linha($linha->id)->ordenados()->get();

        return view('painel.linhas.imagens.index', compact('imagens', 'linha'));
    }

    public function show(Linha $linha, LinhaImagem $imagem)
    {
        return $imagem;
    }

    public function create(Linha $linha)
    {
        return view('painel.linhas.imagens.create', compact('linha'));
    }

    public function store(Linha $linha, LinhasImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['linha_id'] = $linha->id;

            $imagem = LinhaImagem::create($input);
            $view = view('painel.linhas.imagens.imagem', compact('linha', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function update(LinhasImagensRequest $request, Linha $linha, LinhaImagem $imagem)
    {
        try {

            $imagem->update([
                'legenda' => $request->get('legenda'),
                'produto' => $request->get('produto')
            ]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Linha $linha, LinhaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.linha-girante.linhas.imagens.index', $linha)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
