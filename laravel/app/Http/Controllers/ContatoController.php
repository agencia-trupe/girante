<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Contato;
use Illuminate\Http\Request;
use App\Models\ContatoRecebido;
use App\Http\Controllers\Controller;

class ContatoController extends Controller
{

    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function envio(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required'
        ]);

        ContatoRecebido::create($request->all());

        $contato = \App\Models\Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return \Redirect::back()->with('success', true);
    }

}
