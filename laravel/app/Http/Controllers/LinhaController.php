<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Linha;
use App\Models\Produto;
use App\Models\ProdutoImagem;
use App\Models\ProdutoCategoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LinhaController extends Controller
{
    public function index(ProdutoCategoria $categoria_selecionada)
    {
        $linhas = Linha::with('imagens')->ordenados()->get();
        $categorias = ProdutoCategoria::ordenados()->get();
        $categorias_select = ProdutoCategoria::ordenados()->lists('titulo', 'slug');

        if (!$categoria_selecionada->id) {
            $imagens = ProdutoImagem::orderByRaw('RAND()')->take(16)->get();
        } else {
            $imagens = ProdutoImagem::join('produtos as produto', 'produto.id', '=', 'produto_id')
                ->where('produto.produtos_categoria_id', $categoria_selecionada->id)
                ->orderBy('produto.ordem', 'ASC')
                ->orderBy('produto.id', 'DESC')
                ->select('produtos_imagens.*')
                ->ordenados()->get();
        }

        return view('frontend.linha-girante.index', compact('linhas', 'categorias', 'categorias_select', 'categoria_selecionada', 'imagens'));
    }

    public function show(ProdutoCategoria $categoria_selecionada, Produto $produto, $imagem_selecionada = null) {
        $categorias = ProdutoCategoria::ordenados()->get();
        $imagem_selecionada = ProdutoImagem::find($imagem_selecionada) ?: ProdutoImagem::ordenados()->first();

        return view('frontend.linha-girante.show', compact('categorias', 'produto', 'imagem_selecionada'));
    }
}
