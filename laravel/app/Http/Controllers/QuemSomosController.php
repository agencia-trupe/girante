<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Parceiro;
use App\Models\QuemSomos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuemSomosController extends Controller
{

    public function index()
    {
        $quemsomos = QuemSomos::first();
        $parceiros = Parceiro::ordenados()->get();

        return view('frontend.quem-somos', compact('quemsomos', 'parceiros'));
    }

}
