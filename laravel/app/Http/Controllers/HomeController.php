<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Banner;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('banners'));
    }

}
