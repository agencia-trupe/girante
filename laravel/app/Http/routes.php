<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('marcenaria-personalizada/{marcenaria_categoria?}', ['as' => 'marcenaria-personalizada', 'uses' => 'MarcenariaController@index']);

Route::get('linha-girante/{linha_categoria?}', ['as' => 'linha-girante', 'uses' => 'LinhaController@index']);
Route::get('linha-girante/{linha_categoria}/{linha_produto}/{linha_imagem?}', ['as' => 'linha-girante.produto', 'uses' => 'LinhaController@show']);

Route::get('quem-somos', ['as' => 'quem-somos', 'uses' => 'QuemSomosController@index']);

Route::get('novidades/data/{ano}/{mes?}', ['as' => 'novidades.data', 'uses' => 'BlogController@data']);
Route::get('novidades/{blog_categoria?}', ['as' => 'novidades', 'uses' => 'BlogController@index']);
Route::get('novidades/{blog_categoria}/{blog_post}', ['as' => 'novidades.post', 'uses' => 'BlogController@show']);
Route::post('novidades/{blog_categoria}/{blog_post}', ['as' => 'novidades.comentario', 'uses' => 'BlogController@comentario']);

Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);

    Route::resource('banners', 'BannersController');

    Route::group(['prefix' => 'marcenaria-personalizada'], function() {
        Route::resource('projetos', 'ProjetosController');
        Route::resource('projetos.imagens', 'ProjetosImagensController');
        Route::resource('imagens/categorias', 'MarcenariaController');
        Route::resource('imagens', 'MarcenariaImagensController');
    });

    Route::group(['prefix' => 'linha-girante'], function() {
        Route::resource('linhas', 'LinhasController');
        Route::resource('linhas.imagens', 'LinhasImagensController');
        Route::resource('produtos/categorias', 'ProdutosCategoriasController');
        Route::resource('produtos', 'ProdutosController');
        Route::resource('produtos.imagens', 'ProdutosImagensController');
    });

    Route::resource('quem-somos/parceiros', 'ParceirosController');
    Route::resource('quem-somos', 'QuemSomosController');

    Route::resource('novidades/categorias', 'BlogCategoriasController');
    Route::resource('novidades/comentarios', 'BlogComentariosController');
    Route::resource('novidades', 'BlogPostsController');
    Route::get('novidades/comentarios/{comentarios}/aprovacao/{value}', 'BlogComentariosController@aprovacao');
    Route::post('ckeditor-upload', 'BlogPostsController@imageUpload');

    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');

    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
