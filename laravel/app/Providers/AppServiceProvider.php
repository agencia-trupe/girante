<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.common.template', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.blog.*', function($view) {
            $datas = \App\Models\BlogPost::select(\DB::raw('YEAR(data) as ano, MONTH(data) as mes'))
                   ->groupBy('ano', 'mes')
                   ->orderBy('ano', 'DESC')
                   ->orderBy('mes', 'DESC')
                   ->get();

            $view->with('categorias_list', \App\Models\BlogCategoria::has('posts')->ordenados()->get());
            $view->with('datas', $datas);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
