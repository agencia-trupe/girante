<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');

        $router->model('quem-somos', 'App\Models\QuemSomos');
        $router->model('parceiros', 'App\Models\Parceiro');

        $router->model('projetos', 'App\Models\Projeto');
        $router->model('linhas', 'App\Models\Linha');
        $router->model('produtos', 'App\Models\Produto');

        $router->model('novidades', 'App\Models\BlogPost');
        $router->model('comentarios', 'App\Models\BlogComentario');

        $router->model('contato', 'App\Models\Contato');
        $router->model('recebidos', 'App\Models\ContatoRecebido');

        $router->bind('categorias', function($id, $route, $model = null) {
            if (str_is('*marcenaria-personalizada*', $route->getPrefix())) {
                $model = \App\Models\Marcenaria::find($id);
            } elseif (str_is('*produtos*', $route->getPrefix())) {
                $model = \App\Models\ProdutoCategoria::find($id);
            } elseif (str_is('*novidades*', $route->getPrefix())) {
                $model = \App\Models\BlogCategoria::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->bind('imagens', function($id, $route, $model = null) {
            if ($route->hasParameter('projetos')) {
                $model = \App\Models\ProjetoImagem::find($id);
            } elseif ($route->hasParameter('linhas')) {
                $model = \App\Models\LinhaImagem::find($id);
            } elseif ($route->hasParameter('produtos')) {
                $model = \App\Models\ProdutoImagem::find($id);
            } elseif ($route->hasParameter('imagens')) {
                $model = \App\Models\MarcenariaImagem::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->model('usuarios', 'App\Models\User');

        $router->bind('marcenaria_categoria', function($value) {
            return \App\Models\Marcenaria::with('imagens')->slug($value)->first() ?: \App::abort('404');
        });

        $router->bind('linha_categoria', function($value) {
            return \App\Models\ProdutoCategoria::slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('linha_produto', function($value) {
            return \App\Models\Produto::with('imagens')->slug($value)->first() ?: \App::abort('404');
        });

        $router->bind('blog_categoria', function($value) {
            return \App\Models\BlogCategoria::slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('blog_post', function($value) {
            return \App\Models\BlogPost::with('categoria', 'comentariosAprovados')->slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
