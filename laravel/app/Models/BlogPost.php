<?php

namespace App\Models;

use Carbon\Carbon;
use App\Helpers\Tools;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class BlogPost extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'blog_posts';

    protected $guarded = ['id'];

    protected $appends = ['data_formatada'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('blog_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\BlogCategoria', 'blog_categoria_id');
    }

    public function comentarios()
    {
        return $this->hasMany('App\Models\BlogComentario', 'blog_post_id')->orderBy('created_at', 'DESC');
    }

    public function comentariosAprovados()
    {
        return $this->hasMany('App\Models\BlogComentario', 'blog_post_id')->orderBy('created_at', 'DESC')->where('aprovado', 1);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getDataFormatadaAttribute()
    {
        return Tools::formataDataBlog($this->data);
    }
}
