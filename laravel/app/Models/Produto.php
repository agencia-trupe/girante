<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function linha()
    {
        return $this->belongsTo('App\Models\Linha', 'linha_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }
}
