<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function produto_parent()
    {
        return $this->belongsTo('App\Models\Produto', 'produto_id');
    }
}
