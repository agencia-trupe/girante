<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinhaImagem extends Model
{
    protected $table = 'linhas_imagens';

    protected $guarded = ['id'];

    public function scopeLinha($query, $id)
    {
        return $query->where('linha_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
