<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcenariaImagem extends Model
{
    protected $table = 'marcenaria_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function getCategoriasListAttribute()
    {
        return $this->categorias->lists('id')->toArray();
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Models\Marcenaria', 'marcenaria_categoria_imagem', 'imagem_id', 'categoria_id')->ordenados();
    }
}
