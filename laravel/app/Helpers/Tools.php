<?php

namespace App\Helpers;

class Tools
{

    public static function mesExtenso($mes = null)
    {
        $meses = array(
            '1'  => 'Janeiro',
            '2'  => 'Fevereiro',
            '3'  => 'Março',
            '4'  => 'Abril',
            '5'  => 'Maio',
            '6'  => 'Junho',
            '7'  => 'Julho',
            '8'  => 'Agosto',
            '9'  => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        );

        if ($mes) {
            return $meses[$mes];
        }
    }

    public static function formataDataBlog($data = null)
        {
            $meses = array(
                '01' => 'Jan',
                '02' => 'Fev',
                '03' => 'Mar',
                '04' => 'Abr',
                '05' => 'Mai',
                '06' => 'Jun',
                '07' => 'Jul',
                '08' => 'Ago',
                '09' => 'Set',
                '10' => 'Out',
                '11' => 'Nov',
                '12' => 'Dez'
            );

            if ($data) {
                list($dia, $mes, $ano) = explode('/', $data);
                return $dia.' '.$meses[$mes].' '.$ano;
            }
        }

}
