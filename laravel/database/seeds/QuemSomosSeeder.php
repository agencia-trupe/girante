<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quem_somos') ->insert([
            'texto'  => 'quem somos',
            'imagem' => '',
        ]);
    }
}
