<?php

use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato') ->insert([
            'telefone'   => '+55 11 2738·4166',
            'email'      => 'contato@girante.com.br',
            'endereco'   => '<p>Rua Romilda Margarida Gabriel, 162</p><p>Itaim Bibi - São Paulo - SP</p>',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.7475932903526!2d-46.674049848740204!3d-23.577507084599663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce576049fb239f%3A0x33faf736613cacd8!2sR.+Romilda+Margarida+Gabriel%2C+162+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04530-090!5e0!3m2!1spt-BR!2sbr!4v1446036605342" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook'   => 'https://www.facebook.com/girantemoveis',
        ]);
    }
}
