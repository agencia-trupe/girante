<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinhasImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linhas_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('linha_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('produto');
            $table->text('legenda');
            $table->timestamps();
        });

        Schema::table('linhas_imagens', function (Blueprint $table) {
            $table->foreign('linha_id')->references('id')->on('linhas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('linhas_imagens');
    }
}
