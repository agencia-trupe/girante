<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarcenariaImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marcenaria_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->text('legenda');
            $table->timestamps();
        });

        Schema::create('marcenaria_categoria_imagem', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned()->index();
            $table->integer('imagem_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('marcenaria_categoria_imagem', function (Blueprint $table) {
            $table->foreign('categoria_id')->references('id')->on('marcenaria')->onDelete('cascade');
            $table->foreign('imagem_id')->references('id')->on('marcenaria_imagens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('marcenaria_categoria_imagem');
        Schema::drop('marcenaria_imagens');
    }
}
