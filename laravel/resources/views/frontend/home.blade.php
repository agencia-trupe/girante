@extends('frontend.common.template')

@section('content')

    <div class="banners-home">
        @foreach($banners as $banner)
        <a href="{{ $banner->link }}" class="banner" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}');">
            <div class="center">
                <div class="banner-texto">
                    <div>
                        <p class="banner-titulo">{!! $banner->titulo !!}</p>
                        <p class="banner-frase">{!! $banner->frase !!}</p>
                    </div>
                </div>
            </div>
        </a>
        @endforeach

        {{-- FAIXA LARANJA OUTLET
        <a href="{{ route('linha-girante') }}" class="outlet-home">
            <div class="center outlet-home-bg">
                <p class="outlet-titulo">Outlet Girante</p>
                <p class="outlet-frase">Peças de exposição e ponta de estoque à pronta entrega. Escolha a sua »</p>
            </div>
        </a>
        --}}

        <a href="{{ route('contato') }}" class="outlet-home construcard">
            <div class="center outlet-home-bg">
                <img src="{{ asset('assets/img/layout/marca-construcard.png') }}" alt="">
                <p class="outlet-frase">
                    Faça seu projeto com a Girante utilizando seu Construcard.<br>
                    Contate-nos agora!
                </p>
            </div>
        </a>
    </div>


@endsection
