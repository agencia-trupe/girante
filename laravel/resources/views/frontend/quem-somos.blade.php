@extends('frontend.common.template')

@section('content')

    <div class="main quem-somos">
        <div class="center">
            <div class="title">
                <h2>Marcenaria criativa com qualidade superior</h2>
                <h3>a Girante projeta e executa móveis para a vida contemporânea</h3>
            </div>

            <div class="texto">{!! $quemsomos->texto !!}</div>
            <div class="imagem">
                <img src="{{ asset('assets/img/quem-somos/'.$quemsomos->imagem) }}" alt="">
            </div>

            @if(count($parceiros))
            <div class="parceiros">
                <h3>Alguns de nossos parceiros em projetos</h3>
                <div class="parceiros-thumbs">
                    @foreach($parceiros as $parceiro)
                    <a @if($parceiro->link) href="{{ $parceiro->link }}" target="_blank" @endif>
                        <img src="{{ asset('assets/img/quem-somos/parceiros/'.$parceiro->imagem) }}" alt="">
                        <p>{{ $parceiro->nome }}</p>
                        <p>{{ $parceiro->cargo }}</p>
                    </a>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>

@endsection
