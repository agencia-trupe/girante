@extends('frontend.common.template')

@section('content')

    <div class="main novidades">
        <div class="center">
            <div class="title">
                <h2>Novidades em Marcenaria e Mobiliário</h2>
                <h3>acompanhe a Girante na mídia · ideias para inteiores · tendências, novidades e mais!</h3>
            </div>

            @include('frontend.blog.sidebar')

            <div class="novidades-main novidades-post">
                <h3>{{ $post->titulo }}</h3>
                <div class="info">
                    <span class="left">
                        {{ Tools::formataDataBlog($post->data) }} · <a href="{{ route('novidades', $post->categoria->slug) }}">{{ $post->categoria->titulo }}</a>
                    </span>
                    <span class="right">
                        {{ count($post->comentariosAprovados) }} Comentário{{ (count($post->comentariosAprovados) != 1 ? 's' : '') }} · <a href="#" id="comente">Comente!</a>
                    </span>
                </div>

                <div class="texto">
                    {!! $post->texto !!}
                </div>

                <div id="comentarios">
                    <h5>COMENTÁRIOS [{{ count($post->comentariosAprovados) }}]</h5>

                    <form action="" id="form-comentario" data-url="{{ Request::url() }}">
                        <input type="text" name="autor" id="autor" placeholder="NOME" required>
                        <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                        <textarea name="comentario" id="comentario" placeholder="COMENTÁRIO" required></textarea>
                        <input type="hidden" name="blog_id" id="blog_id" value="{{ $post->id }}">
                        <input type="submit" value="ENVIAR">
                        <p>Os comentários são moderados pela nossa equipe que pode optar por não exibí-los se assim achar conveniente, não cabendo ao autor do comentário qualquer alegação.</p>
                        <div id="form-resposta"></div>
                    </form>

                    <ul class="comentarios-lista">
                        @foreach($post->comentariosAprovados as $comentario)
                            <li>
                                <span class="autor">
                                    {{ Tools::formataDataBlog($comentario->created_at) }} · {{{ $comentario->autor }}}
                                </span>
                                <span class="comentario">
                                    {{{ $comentario->comentario }}}
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="paginacao">
                    @if($anterior)
                    <a href="{{ route('novidades.post', [$anterior->categoria->slug, $anterior->slug]) }}" class="prev">Post Anterior</a>
                    @endif
                    <a href="#" id="topo">Topo</a>
                    @if($proximo)
                    <a href="{{ route('novidades.post', [$proximo->categoria->slug, $proximo->slug]) }}" class="next">Próximo Post</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection