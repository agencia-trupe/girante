            <div class="novidades-sidebar">
                <div class="novidades-categorias">
                    @foreach($categorias_list as $cat)
                    <a href="{{ route('novidades', $cat->slug) }}" @if(isset($categoria) && $cat->id == $categoria->id && Route::currentRouteName() == 'novidades') class="active" @endif>» {{ $cat->titulo }}</a>
                    @endforeach
                </div>

                <div class="novidades-datas">
                    <?php $ano = null ?>
                    @foreach($datas as $data)
                        @if($ano != $data->ano)
                            <a href="{{ route('novidades.data', $data->ano) }}" class="ano @if($data->ano == $ano_selecionado) active @endif">
                                {{ $data->ano }}
                            </a>
                            <?php $ano = $data->ano ?>
                        @endif
                        <a href="{{ route('novidades.data', [$data->ano, $data->mes]) }}" class="mes @if($data->ano == $ano_selecionado && $data->mes == $mes_selecionado) active @endif">
                            [{{ Tools::mesExtenso($data->mes) }}]
                        </a>
                    @endforeach
                </div>
            </div>