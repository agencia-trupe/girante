@extends('frontend.common.template')

@section('content')

    <div class="main novidades">
        <div class="center">
            <div class="title">
                <h2>Novidades em Marcenaria e Mobiliário</h2>
                <h3>acompanhe a Girante na mídia · ideias para interiores · tendências, novidades e mais!</h3>
            </div>

            @include('frontend.blog.sidebar')

            <div class="novidades-main novidades-posts">
                <div id="novidades-posts-list" data-url="{{ Request::url() }}" data-capa="{{ asset('assets/img/blog/') }}" data-link="{{ route('novidades') }}">
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>
                </div>
                <a href="#" id="novidades-load-more" class="loading">Carregar mais</a>
                <script>var current_page = 0;</script>
            </div>
        </div>
    </div>

@endsection
