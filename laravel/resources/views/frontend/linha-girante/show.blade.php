@extends('frontend.common.template')

@section('content')

    <div class="main linha">
        <div class="center">
            <div class="title title-produto">
                <h2>Linha própria de móveis com estilo industrial</h2>
                <h3>pronta entrega ou sob medida</h3>
            </div>

            <div class="linha-produto-info">
                <h3>{{ $produto->linha->titulo }}</h3>
                <h2>{{ $produto->titulo }}</h2>
                <p>{{ $produto->descricao }}</p>
                <a href="{{ route('contato') }}" class="botao-laranja">encomende!</a>
            </div>

            <div class="linha-produto-imagens">
                <div id="linha-produto-imagem">
                    <img src="{{ asset('assets/img/produtos/imagens/'.$imagem_selecionada->imagem) }}" alt="">
                    <div class="loading"></div>
                </div>
                <div class="linha-produto-thumbs">
                    @foreach($produto->imagens as $imagem)
                    <a href="javascript:void(0);" data-imagem="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" data-id="{{ $imagem->id }}" @if($imagem_selecionada->id == $imagem->id) class='active' @endif>
                        <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="" id="linha-produto-imagem">
                    </a>
                    @endforeach
                </div>
            </div>

            <div class="linha-categorias-footer">
                <p>Conheça outros produtos da Linha Girante</p>
                @foreach($categorias as $categoria)
                <a href="{{ route('linha-girante', $categoria->slug) }}">{{ $categoria->titulo }}</a>
                @endforeach
            </div>
        </div>
    </div>

@endsection