@extends('frontend.common.template')

@section('content')

    <div class="main linha">
        <div class="center">
            <div class="title">
                <h2>Linha própria de móveis com estilo industrial</h2>
                <h3>pronta entrega ou sob medida</h3>
            </div>

            @foreach($linhas as $linha)
            @if(count($linha->imagens))
            <h4>{{ $linha->titulo }}</h4>
            <div class="linhas galeria-carousel">
                @foreach($linha->imagens as $imagem)
                <a href="{{ asset('assets/img/linhas/imagens/'.$imagem->imagem) }}" class="fancybox galeria-carousel-thumb" rel="linha{{ $linha->id }}" title="{{ $imagem->legenda }}" data-legendaextra="<span class='legenda-uppercase'>{{ $imagem->produto }}</span> · {{ $linha->titulo }}">
                    <img src="{{ asset('assets/img/linhas/thumbs/'.$imagem->imagem) }}" alt="">
                    <div class="overlay-wrapper">
                        <div class="overlay">
                            <div>
                                <p class="overlay-titulo">{{ $imagem->produto }}</p>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            @endif
            @endforeach

            <h5>A Linha Girante é uma linha de móveis inteligente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio unde eum, ex dolorem eos at rem quas corporis! Nemo, hic!</h5>

            <div class="linha-produtos galeria-produtos">
                <div class="galeria-produtos-categorias">
                    @foreach($categorias as $categoria)
                    <a href="{{ route('linha-girante', $categoria->slug) }}" @if($categoria->id === $categoria_selecionada->id) class="active" @endif>{{ $categoria->titulo }}</a>
                    @endforeach
                    {!! Form::select('filtro', $categorias_select, $categoria_selecionada->slug, ['id' => 'filtro-categorias-frontend', 'placeholder' => 'Selecione uma Categoria', 'data-route' => 'linha-girante']) !!}
                    <a href="{{ route('contato') }}" class="botao-laranja">encomende!</a>
                </div>

                <div class="galeria-produtos-thumbs">
                    @foreach($imagens as $imagem)
                    <a href="{{ route('linha-girante.produto', [$imagem->produto_parent->categoria->slug, $imagem->produto_parent->slug, $imagem->id]) }}">
                        <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                        <div class="overlay"></div>
                    </a>
                    @endforeach
                </div>
                <div class="linha-links">
                    <a href="{{ route('contato') }}">Entre em contato e encomende a sua peça »</a>
                </div>
            </div>
        </div>
    </div>

@endsection
