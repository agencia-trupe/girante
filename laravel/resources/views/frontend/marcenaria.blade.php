@extends('frontend.common.template')

@section('content')

    <div class="main marcenaria">
        <div class="center">
            <div class="title">
                <h2>Executamos projetos de mobiliário personalizados</h2>
                <h3>com criação própria ou de outros escritórios</h3>
            </div>

            <div class="marcenaria-projetos galeria-carousel">
                @foreach($projetos as $projeto)
                <a href="javascript:void(0);" class="fancybox-thumb galeria-carousel-thumb" data-galeria="projeto{{ $projeto->id }}">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->imagem) }}" alt="">
                    <div class="overlay-wrapper">
                        <div class="overlay">
                            <div>
                                <p class="overlay-titulo">{{ $projeto->titulo }}</p>
                                <p class="overlay-arquiteto">{{ $projeto->arquiteto }}</p>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="marcenaria-projetos-galerias hidden">
                @foreach($projetos as $projeto)
                    @foreach($projeto->imagens as $imagem)
                    <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="projeto{{ $projeto->id }}" title="{{ $imagem->legenda }}" data-legendaextra="<span class='legenda-uppercase'>{{ $projeto->titulo }}</span> · {{ $projeto->arquiteto }}"></a>
                    @endforeach
                @endforeach
            </div>

            <div class="marcenaria-produtos galeria-produtos">
                <div class="galeria-produtos-categorias">
                    @foreach($categorias as $categoria)
                    <a href="{{ route('marcenaria-personalizada', $categoria->slug) }}" @if($categoria->id === $categoria_selecionada->id) class="active" @endif>{{ $categoria->titulo }}</a>
                    @endforeach
                    {!! Form::select('filtro', $categorias_select, $categoria_selecionada->slug, ['id' => 'filtro-categorias-frontend', 'placeholder' => 'Selecione uma Categoria', 'data-route' => 'marcenaria-personalizada']) !!}
                </div>

                <div class="galeria-produtos-thumbs">
                    @foreach($imagens as $imagem)
                    <a href="{{ asset('assets/img/marcenaria/imagens/'.$imagem->imagem) }}" class="fancybox" rel="marcenaria-produtos" title="{{ $imagem->legenda }}">
                        <img src="{{ asset('assets/img/marcenaria/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                        <div class="overlay"></div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
