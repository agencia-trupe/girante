<a href="{{ route('marcenaria-personalizada') }}" @if(str_is('marcenaria-personalizada*', Route::currentRouteName())) class='active' @endif>Marcenaria Personalizada</a>
<a href="{{ route('linha-girante') }}" @if(str_is('linha-girante*', Route::currentRouteName())) class='active' @endif>Linha Girante</a>
<a href="{{ route('quem-somos') }}" @if(str_is('quem-somos*', Route::currentRouteName())) class='active' @endif>Quem Somos</a>
<a href="{{ route('novidades') }}" @if(str_is('novidades*', Route::currentRouteName())) class='active' @endif>Novidades</a>
<a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
@if($contato->facebook)<a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>@endif
