    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ config('site.name') }} · Todos os direitos reservados ·
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
