    <header @if(Route::currentRouteName() === 'home') class="home" @endif>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>

            <nav id="nav-desktop">
                @include('frontend.common._nav')
            </nav>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
