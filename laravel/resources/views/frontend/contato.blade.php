@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="title">
                <h2>Girante Móveis</h2>
                <h3>serviços de marcenaria, projeto e execução</h3>
            </div>

            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <div class="endereco">{!! $contato->endereco !!}</div>
            </div>

            <form action="{{ route('contato.envio') }}" method="POST" id="form-contato">
                {{ csrf_field() }}

                @if(session('success'))
                <div class="response success">
                    Mensagem enviada com sucesso!
                </div>
                @elseif (count($errors) > 0)
                <div class="response error">
                    Preencha corretamente os campos obrigatórios<span>(*)</span>.
                </div>
                @endif

                <input type="text" name="nome" id="nome" placeholder="nome" value="{{ old('nome') }}" @if($errors->has('nome'))class="error"@endif>
                <input type="text" name="telefone" id="telefone" placeholder="telefone" value="{{ old('telefone') }}" @if($errors->has('telefone'))class="error"@endif>
                <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" @if($errors->has('email'))class="error"@endif>
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" @if($errors->has('mensagem'))class="error"@endif>{{ old('mensagem') }}</textarea>
                <input type="submit" value="enviar">
            </form>
        </div>

        <div class="googlemaps">{!! $contato->googlemaps !!}</div>
    </div>

@endsection
