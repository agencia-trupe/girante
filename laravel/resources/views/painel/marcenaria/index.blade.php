@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Marcenaria Personalizada /</small> Imagens
            <div class="btn-group pull-right">
                <a href="{{ route('painel.marcenaria-personalizada.imagens.categorias.index') }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Editar Categorias</a>
                <a href="{{ route('painel.marcenaria-personalizada.imagens.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Adicionar Imagem</a>
            </div>
        </h2>
    </legend>

    <div id="imagens" data-table="marcenaria_imagens">
    @if(!count($imagens))
        <div class="alert alert-warning no-images" role="alert">Nenhuma imagem cadastrada.</div>
    @else
        @foreach($imagens as $imagem)
        @include('painel.marcenaria.imagem')
        @endforeach
    @endif
    </div>

@endsection
