@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcenaria Personalizada /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.marcenaria-personalizada.imagens.store', 'files' => true]) !!}

        @include('painel.marcenaria.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
