@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcenaria Personalizada / Imagens /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.marcenaria-personalizada.imagens.categorias.store']) !!}

        @include('painel.marcenaria.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
