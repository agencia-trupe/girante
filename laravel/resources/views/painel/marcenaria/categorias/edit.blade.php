@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcenaria Personalizada / Imagens /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($marcenaria, [
        'route'  => ['painel.marcenaria-personalizada.imagens.categorias.update', $marcenaria->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marcenaria.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
