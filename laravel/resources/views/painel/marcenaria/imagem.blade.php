<div class="imagem col-md-2 col-sm-3 col-xs-4" style="margin:5px 0;position:relative;padding:0 5px;" id="{{ $imagem->id }}">
    <img src="{{ url('assets/img/marcenaria/imagens/thumbs/'.$imagem->imagem) }}" alt="" style="display:block;width:100%;height:auto;cursor:move;">
    {!! Form::open([
        'route'  => ['painel.marcenaria-personalizada.imagens.destroy', $imagem->id],
        'method' => 'delete'
    ]) !!}

    <div class="btn-group btn-group-sm" style="position:absolute;bottom:8px;left:10px;">
        <a href="{{ route('painel.marcenaria-personalizada.imagens.edit', $imagem) }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span></a>
        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove"></span></button>
    </div>

    {!! Form::close() !!}
</div>
