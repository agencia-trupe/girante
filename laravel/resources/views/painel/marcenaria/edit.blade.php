@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcenaria Personalizada /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($imagem, [
        'route'  => ['painel.marcenaria-personalizada.imagens.update', $imagem->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marcenaria.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
