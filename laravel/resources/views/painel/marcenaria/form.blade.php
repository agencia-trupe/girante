@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categorias_list', 'Categorias') !!}
    @if(sizeof($categorias))
        {!! Form::select('categorias_list[]', $categorias, null, ['class' => 'form-control multi-select', 'multiple']) !!}
    @else
        <div class="alert alert-info" role="alert" style="margin:5px 0 0;">Nenhuma categoria cadastrada.</div>
    @endif
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/marcenaria/imagens/'.$imagem->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.marcenaria-personalizada.imagens.index') }}" class="btn btn-default btn-voltar">Voltar</a>
