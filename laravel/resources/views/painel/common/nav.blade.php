<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>

    <li class="dropdown @if(str_is('painel.marcenaria-personalizada*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Marcenaria Personalizada
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.marcenaria-personalizada.projetos.index') }}">Projetos</a></li>
            <li><a href="{{ route('painel.marcenaria-personalizada.imagens.index') }}">Imagens</a></li>
        </ul>
    </li>

    <li class="dropdown @if(str_is('painel.linha-girante*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Linha Girante
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.linha-girante.linhas.index') }}">Linhas</a></li>
            <li><a href="{{ route('painel.linha-girante.produtos.index') }}">Produtos</a></li>
        </ul>
    </li>

    <li @if(str_is('painel.quem-somos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
    </li>

    <li @if(str_is('painel.novidades*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.novidades.index') }}">Novidades</a>
    </li>

    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
