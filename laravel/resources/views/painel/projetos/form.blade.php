@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$projeto->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 245px;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('arquiteto', 'Arquiteto') !!}
    {!! Form::text('arquiteto', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.marcenaria-personalizada.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
