@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha Girante /</small> Adicionar Linha</h2>
    </legend>

    {!! Form::open(['route' => 'painel.linha-girante.linhas.store', 'files' => true]) !!}

        @include('painel.linhas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
