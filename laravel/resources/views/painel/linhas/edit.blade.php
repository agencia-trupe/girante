@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha Girante /</small> Editar Linha</h2>
    </legend>

    {!! Form::model($linha, [
        'route'  => ['painel.linha-girante.linhas.update', $linha->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.linhas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
