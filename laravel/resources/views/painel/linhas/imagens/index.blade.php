@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.linha-girante.linhas.index') }}" title="Voltar para Linhas" class="btn btn-sm btn-default">
        &larr; Voltar para Linhas
    </a>

    <legend>
        <h2>
            <small>Linha Girante / Imagens da Linha:</small> {{ $linha->titulo }}

            {!! Form::open(['route' => ['painel.linha-girante.linhas.imagens.store', $linha->id], 'files' => true, 'class' => 'pull-right']) !!}
                <span class="btn btn-success btn-sm" style="position:relative;overflow:hidden">
                    <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>
                    Adicionar Imagens
                    <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
                </span>
            {!! Form::close() !!}
        </h2>
    </legend>

    <div class="progress progress-striped active">
        <div class="progress-bar" style="width: 0"></div>
    </div>

    <div class="alert alert-block alert-danger errors" style="display:none"></div>

    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <span class="glyphicon glyphicon-move" style="margin-right: 10px;"></span>
            Clique e arraste as imagens para ordená-las.
        </small>
    </div>

    <div id="imagens" data-table="linhas_imagens">
    @if(!count($imagens))
        <div class="alert alert-warning no-images" role="alert">Nenhuma imagem cadastrada.</div>
    @else
        @foreach($imagens as $imagem)
        @include('painel.linhas.imagens.imagem')
        @endforeach
    @endif
    </div>

@endsection
