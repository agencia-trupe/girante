@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha Girante / Produtos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.linha-girante.produtos.categorias.store']) !!}

        @include('painel.produtos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
