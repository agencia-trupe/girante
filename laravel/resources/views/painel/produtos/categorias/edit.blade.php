@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha Girante / Produtos /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.linha-girante.produtos.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.produtos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
