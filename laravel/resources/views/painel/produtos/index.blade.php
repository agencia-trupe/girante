@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Linha Girante /</small> Produtos
            <a href="{{ route('painel.linha-girante.produtos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Input::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/linha-girante/produtos']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.linha-girante.produtos.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os produtos.
            </p>
        </div>
        @endif
    </div>

    @if(!count($produtos))
    <div class="alert alert-warning" role="alert">Nenhum produto cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Título</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($produtos as $produto)
            <tr class="tr-row" id="{{ $produto->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                @if(!$filtro)
                <td>
                    @if($produto->categoria){{ $produto->categoria->titulo }}@endif
                </td>
                @endif
                <td>{{ $produto->titulo }}</td>
                <td><a href="{{ route('painel.linha-girante.produtos.imagens.index', $produto->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.linha-girante.produtos.destroy', $produto->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.linha-girante.produtos.edit', $produto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
