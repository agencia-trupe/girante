@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha Girante /</small> Adicionar Produto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.linha-girante.produtos.store']) !!}

        @include('painel.produtos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
