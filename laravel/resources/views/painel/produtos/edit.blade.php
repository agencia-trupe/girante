@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha Girante /</small> Editar Produto</h2>
    </legend>

    {!! Form::model($produto, [
        'route' => ['painel.linha-girante.produtos.update', $produto->id],
        'method' => 'patch'])
    !!}

        @include('painel.produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
