@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Quem Somos
            <a href="{{ route('painel.quem-somos.parceiros.index') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-user" style="margin-right:10px;"></span>Editar Parceiros</a>
        </h2>
    </legend>

    {!! Form::model($quemsomos, [
        'route'  => ['painel.quem-somos.update', $quemsomos->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
