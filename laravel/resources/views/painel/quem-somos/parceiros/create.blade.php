@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Parceiro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.quem-somos.parceiros.store', 'files' => true]) !!}

        @include('painel.quem-somos.parceiros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
