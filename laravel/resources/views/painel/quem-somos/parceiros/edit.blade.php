@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Parceiro</h2>
    </legend>

    {!! Form::model($parceiro, [
        'route'  => ['painel.quem-somos.parceiros.update', $parceiro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.parceiros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
