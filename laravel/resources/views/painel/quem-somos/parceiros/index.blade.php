@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.quem-somos.index') }}" class="btn btn-sm btn-default" style="margin-bottom:10px;">&larr; Voltar para Quem Somos</a>

    <legend>
        <h2>
            Parceiros
            <a href="{{ route('painel.quem-somos.parceiros.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Parceiro</a>
        </h2>
    </legend>

    @if(!count($parceiros))
    <div class="alert alert-warning" role="alert">Nenhum parceiro cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="parceiros">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Nome</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($parceiros as $parceiro)
            <tr class="tr-row" id="{{ $parceiro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $parceiro->nome }}</td>
                <td><img src="{{ url('assets/img/quem-somos/parceiros/'.$parceiro->imagem) }}" alt="" style="width:100%;max-width:100px;height:auto;"></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.quem-somos.parceiros.destroy', $parceiro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.quem-somos.parceiros.edit', $parceiro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
