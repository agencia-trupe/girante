@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Novidades
            <div class="btn-group pull-right">
                <a href="{{ route('painel.novidades.comentarios.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-comment" style="margin-right:10px;"></span>Comentários</a>
                <a href="{{ route('painel.novidades.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Adicionar Post</a>
            </div>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Input::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/novidades']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.novidades.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
    </div>

    @if(!count($posts))
    <div class="alert alert-warning" role="alert">Nenhum post cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Título</th>
                <th>Categoria</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($posts as $post)
            <tr class="tr-row">
                <td>{{ $post->data }}</td>
                <td>
                    {{ $post->titulo }}
                    @if($post->destaque_home)
                    <span class="label label-warning" style="margin-left:3px;">destaque</span>
                    @endif
                </td>
                <td>@if($post->categoria){{ $post->categoria->titulo }}@endif</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.novidades.destroy', $post->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.novidades.edit', $post->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $posts->appends(['filtro' => Input::get('filtro')])->render() !!}
    @endif

@endsection
