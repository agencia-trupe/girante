@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Novidades /</small> Editar Post</h2>
    </legend>

    {!! Form::model($post, [
        'route' => ['painel.novidades.update', $post->id],
        'method' => 'patch',
        'files' => true])
    !!}

        @include('painel.blog.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
