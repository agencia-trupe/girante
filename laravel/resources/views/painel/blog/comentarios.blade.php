@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.novidades.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Novidades
    </a>

    <legend>
        <h2>
            <small>Novidades /</small> Comentários
        </h2>
    </legend>

    @if(!count($comentarios))
    <div class="alert alert-warning" role="alert">Nenhum comentário cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Status</th>
                <th>Post</th>
                <th>Data</th>
                <th>Autor</th>
                <th>E-mail</th>
                <th>Comentário</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($comentarios as $comentario)
            <tr class="tr-row">
                <td>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('aprovado', $comentario->id, $comentario->aprovado, ['class' => 'checkbox-aprovacao']) !!}
                            Publicado
                        </label>
                    </div>
                </td>
                <td><a href="{{ route('novidades.post', [$comentario->post->categoria->slug, $comentario->post->slug]) }}" target="_blank">{{ $comentario->post->titulo }}</a></td>
                <td>{{ $comentario->created_at }}</td>
                <td>{{ $comentario->autor }}</td>
                <td>{{ $comentario->email }}</td>
                <td>{{ $comentario->comentario }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.novidades.comentarios.destroy', $comentario->id), 'method' => 'delete')) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $comentarios->render() !!}
    @endif

@endsection
