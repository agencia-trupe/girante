@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Novidades /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.novidades.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.blog.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
