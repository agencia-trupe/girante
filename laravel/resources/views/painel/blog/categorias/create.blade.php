@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Novidades /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.novidades.categorias.store']) !!}

        @include('painel.blog.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
