@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Novidades /</small> Adicionar Post</h2>
    </legend>

    {!! Form::open(['route' => 'painel.novidades.store', 'files'  => true]) !!}

        @include('painel.blog.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
