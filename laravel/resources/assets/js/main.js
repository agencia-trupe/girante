(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.homeCycle = function() {
        var $slideshow = $('.banners-home');
        if (!$slideshow.length) return;

        $slideshow.cycle({
            fx: 'fade',
            slides: '>a.banner'
        });
    };

    App.galeriaCarousel = function() {
        var $wrapper = $('.galeria-carousel');
        if (!$wrapper.length) return;

        $wrapper.slick({
            arrows: true,
            autoplay: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            accessibility: false,
            responsive: [
                {
                    breakpoint: 1500,
                    settings: { slidesToShow: 4, slidesToScroll: 4 }
                },
                {
                    breakpoint: 768,
                    settings: { slidesToShow: 3, slidesToScroll: 3 }
                }
            ]
        });

        $wrapper.imagesLoaded(function() {
            $wrapper.addClass('images-loaded');
        });
    };

    App.thumbFancybox = function() {
        var $handle = $('.fancybox');
        if (!$handle.length) return;

        $('.fancybox').fancybox({
            margin: [45, 20, 20, 20],
            padding: 10,
            nextSpeed: 500,
            prevSpeed: 500,
            helpers: {
                title: {
                    type: 'inside'
                }
            },
            afterLoad: function() {
                var legendaextra;

                if (this.element[0].dataset !== undefined) {
                    legendaextra = this.element[0].dataset.legendaextra;
                } else {
                    legendaextra = this.element[0].getAttribute('data-legendaextra');
                }

                if (legendaextra !== undefined && legendaextra !== null) {
                    this.title = legendaextra + '<br>' + this.title;
                }
            },
        });

        $('.fancybox-thumb').click(function(e) {
            var el, id = $(this).data('galeria');
            if(id){
                el = $('.fancybox[rel=' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.filtroCategorias = function() {
        $('#filtro-categorias-frontend').on('change', function () {
            var slug  = $(this).val(),
                base  = $('base').attr('href'),
                route = $(this).data('route');

            if (slug) {
                window.location = base + '/' + route + '/' + slug;
            }
        });
    };

    App.produtoThumbs = function() {
        var $wrapper = $('.linha-produto-thumbs');
        if (!$wrapper.length) return;

        $wrapper.on('click', 'a', function(event) {
            event.preventDefault();

            var newSrc = $(this).data('imagem'),
                newId  = $(this).data('id'),
                image  = new Image();

            image.onload = function() {
                $('#linha-produto-imagem img').attr('src', newSrc);
                $('#linha-produto-imagem .loading').removeClass('active');
                var paths = location.pathname.split('/');
                if (!isNaN(parseFloat(paths[paths.length-1])) && isFinite(paths[paths.length-1])) {
                    paths[paths.length-1] = newId;
                } else {
                    paths.push(newId);
                }

                window.history.replaceState("", "", paths.join('/'));
            };

            $('#linha-produto-imagem .loading').addClass('active');
            $wrapper.find('a').removeClass('active');
            $(this).addClass('active');
            image.src = newSrc;
        });
    };

    App.getPostsBlog = function() {
        var $wrapper = $('#novidades-posts-list');
        if (!$wrapper.length) return;

        var postsUrl = $wrapper.data('url'),
            linksUrl = $wrapper.data('link'),
            capaPath = $wrapper.data('capa');

        var displayPosts = function () {
            $('#novidades-load-more').addClass('loading');

            $.ajax({
                type: "GET",
                url: postsUrl,
                data: { 'page': window.current_page + 1 },
                success: function(response) {
                    window.current_page = response.current_page;

                    if (window.current_page == response.last_page || response.last_page === 0) {
                        $('#novidades-load-more').hide();
                    } else {
                        $('#novidades-load-more').show();
                    }

                    $('#novidades-load-more').removeClass('loading');

                    var thumb;

                    $.each(response.data, function(id, post) {
                        thumb  = '<a href="' + linksUrl + '/' + post.categoria.slug + '/' + post.slug + '" class="thumb loading">';
                        thumb += '<img src="' + capaPath + '/' + post.imagem + '" alt="">';
                        thumb += '<div class="overlay">';
                        thumb += '<span class="titulo">' + post.titulo + '</span>';
                        thumb += '<span class="info">' + post.data_formatada + '·' + post.categoria.titulo + '</span>';
                        thumb += '</div></a>';

                        $wrapper.append(thumb);

                        $wrapper.imagesLoaded(function() {
                            $wrapper.masonry('reloadItems');
                            $wrapper.masonry();
                            $wrapper.find('.thumb').removeClass('loading');
                        });
                    });
                },
                error: function(response) {
                    console.log('Erro ao carregar posts:' + response);
                },
                dataType: 'json'
            });
        };

        $wrapper.masonry({
            itemSelector: '.thumb',
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer'
        });

        displayPosts();

        $('#novidades-load-more').click(function(event) {
            event.preventDefault();
            displayPosts();
        });
    };

    App.scrollBlog = function() {
        var $comentarios = $('#comentarios');
        if (!$comentarios.length) return;

        $('#topo').click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });

        $('#comente').click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: $comentarios.offset().top - 30 }, 'slow');
        });
    };

    App.comentarioEnvio = function(event) {
        event.preventDefault();

        var $formComentario = $(this),
            $formComentarioSubmit = $formComentario.find('input[type=submit]'),
            $formComentarioResposta = $formComentario.find('#form-resposta');

        $formComentarioResposta.hide();

        $.ajax({
            type: "POST",
            url: $formComentario.data('url'),
            data: {
            autor      : $('#autor').val(),
            email      : $('#email').val(),
            comentario : $('#comentario').val()
            },
            success: function(data) {
                $formComentario[0].reset();
                $formComentarioResposta.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                var key = Object.keys(data.responseJSON)[0];
                $formComentarioResposta.hide().html(data.responseJSON[key]).fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.homeCycle();
        this.galeriaCarousel();
        this.thumbFancybox();
        this.filtroCategorias();
        this.produtoThumbs();
        this.scrollBlog();
        this.getPostsBlog();
        $('#form-comentario').on('submit', App.comentarioEnvio);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
