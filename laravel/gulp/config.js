exports.development = {
    vhost  : 'girante.dev',
    stylus : './resources/assets/stylus/',
    js     : './resources/assets/js/',
    img    : './resources/assets/img/',
    vendor : './resources/assets/vendor/'
};

exports.build = {
    css : '../public/assets/css/',
    js  : '../public/assets/js/',
    img : '../public/assets/img/layout/'
};

exports.vendorPlugins = [
    this.development.vendor + 'jquery-cycle2/build/jquery.cycle2.min.js',
    this.development.vendor + 'slick-carousel/slick/slick.min.js',
    this.development.vendor + 'fancybox/source/jquery.fancybox.pack.js',
    this.development.vendor + 'imagesloaded/imagesloaded.pkgd.min.js',
    this.development.vendor + 'masonry/dist/masonry.pkgd.min.js',
];
