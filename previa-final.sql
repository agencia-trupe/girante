-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.83
-- Tempo de geração: 15/01/2016 às 10:38
-- Versão do servidor: 5.6.21-69.0-log
-- Versão do PHP: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `trupe1101`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `frase`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, '20151203134755_ArildoAzevedo_053.jpg', 'Marcenaria Personalizada', 'executamos projetos · criação própria ou de outros escritórios', 'http://www.trupe.net/previa-girante/marcenaria-personalizada', '2015-11-09 20:30:43', '2015-12-03 15:47:58'),
(2, 1, '20151109183202_img-home2.jpg', 'Marcenaria Imbatível ', 'projetos de móveis customizados para cada necessidade · excelente acabamento', 'http://www.trupe.net/previa-girante/marcenaria-personalizada', '2015-11-09 20:32:05', '2015-12-03 15:50:44'),
(3, 3, '20151109183252_img-home3.jpg', 'Linha de móveis estilo industrial', ' pronta entrega ou sob medida · alta resistência e durabilidade em um estilo singular', 'http://www.trupe.net/previa-girante/linha-girante', '2015-11-09 20:32:55', '2016-01-07 14:19:26'),
(4, 2, '20151203141828_AAA_3032.JPG', 'Escritório de Advocacia', 'marcenaria com seriedade ', 'http://www.trupe.net/previa-girante/linha-girante', '2015-12-03 16:16:29', '2016-01-05 18:24:58');

-- --------------------------------------------------------

--
-- Estrutura para tabela `blog_categorias`
--

CREATE TABLE IF NOT EXISTS `blog_categorias` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `blog_categorias`
--

INSERT INTO `blog_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Girante na mídia', 'girante-na-midia', '2015-11-10 21:31:20', '2015-11-10 21:31:20');

-- --------------------------------------------------------

--
-- Estrutura para tabela `blog_comentarios`
--

CREATE TABLE IF NOT EXISTS `blog_comentarios` (
`id` int(10) unsigned NOT NULL,
  `blog_post_id` int(10) unsigned NOT NULL,
  `autor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` text COLLATE utf8_unicode_ci NOT NULL,
  `aprovado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
`id` int(10) unsigned NOT NULL,
  `blog_categoria_id` int(10) unsigned DEFAULT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `blog_categoria_id`, `data`, `titulo`, `slug`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, '2013-11-10', 'Marcenaria imbatível', 'marcenaria-imbativel', '20151110193903_135528_e5bbd0c75a5948568644e84f748112a5.jpg_srz_323_429_75_22_0.50_1.20_0.00_jpg_srz.jpg', '<p>Projeto executado pela Girante &eacute; capa da revista Casa e Jardim.</p>\r\n\r\n<p>O projeto de [nome arquiteto] foi todo produzido pela Girante Marcenaria e encantou os editores da revista Casa e Jardim que atribu&iacute;ram um t&iacute;tulo que resume o trabalho da Girante: uma MARCENARIA IMBAT&Iacute;VEL.</p>\r\n\r\n<p>O apartamento contou com cozinha,&nbsp;estar e quarto&nbsp;completos com acabamento em [nome da madeira e materiais].</p>\r\n\r\n<p>Confira mais algumas imagens do projeto:</p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110193831_COZINHA.jpg" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110193850_COZINHA-1.jpg" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110193900_DORM-CASAL.jpg" /></p>\r\n', '2015-11-10 21:39:03', '2015-11-10 21:39:03'),
(2, 1, '2015-07-10', 'Móveis da Girante no programa "Olho Mágico - Reforma de Vizinhos" no GNT', 'moveis-da-girante-no-programa-olho-magico-reforma-de-vizinhos-no-gnt', '20151110195101_reforma-de-vizinhos.png', '<p>Os M&oacute;veis da Linha Girante ajudaram a compor a decora&ccedil;&atilde;o criativa da arquiteta Leila Bittencourt para o programa do GNT &quot;Olho M&aacute;gico -&nbsp;Reforma de Vizinhos&quot;.</p>\r\n\r\n<p>O epis&oacute;dio foi ao ar dia 29 de abril 2015 e pode ser visto completo no site do GNT no link:&nbsp;<a href="http://gnt.globo.com/programas/olho-magico-reforma-de-vizinhos/videos/4212972.htm" target="_blank">http://gnt.globo.com/programas/olho-magico-reforma-de-vizinhos/videos/4212972.htm</a></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200712_reforma-de-vizinhos.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200722_reforma-de-vizinhos2.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200732_reforma-de-vizinhos3.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200743_reforma-de-vizinhos4.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200752_reforma-de-vizinhos5.png" /></p>\r\n', '2015-11-10 21:51:01', '2015-11-24 16:33:20'),
(3, 1, '2014-02-23', 'O estado de São Paulo', 'o-estado-de-sao-paulo', '20151130170237_O-Estado-de-SP---23-02-2014.jpg', '<p>texto....</p>\r\n', '2015-11-30 19:02:39', '2015-11-30 19:05:21'),
(4, 1, '2014-03-01', 'CASA CLAUDIA', 'casa-claudia', '20151201174348_MATÉRIA-CASA-CLAUDIA---MAR-2014-EDIÇÃO-631-PG.jpg', '<p>texto...</p>\r\n', '2015-12-01 19:43:50', '2015-12-01 19:43:50');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
`id` int(10) unsigned NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `telefone`, `email`, `endereco`, `googlemaps`, `facebook`, `created_at`, `updated_at`) VALUES
(1, '+55 11 2738·4166 ou 2738·4167', 'girante@girante.com.br', '<p>Rua Romilda Margarida Gabriel, 162</p>\r\n\r\n<p>Itaim Bibi - S&atilde;o Paulo - SP</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.7475932903526!2d-46.674049848740204!3d-23.577507084599663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce576049fb239f%3A0x33faf736613cacd8!2sR.+Romilda+Margarida+Gabriel%2C+162+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04530-090!5e0!3m2!1spt-BR!2sbr!4v1446036605342" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://www.facebook.com/girantemoveis', '0000-00-00 00:00:00', '2016-01-07 15:59:47');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `telefone`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'Ariildo de Andrade Azevedo', 'arildo@andradeazevedo.com', '11 981933707', 'Favor enviar orçamento do baú.', 1, '2015-12-18 19:57:43', '2015-12-21 18:51:01');

-- --------------------------------------------------------

--
-- Estrutura para tabela `linhas`
--

CREATE TABLE IF NOT EXISTS `linhas` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `linhas`
--

INSERT INTO `linhas` (`id`, `ordem`, `titulo`, `created_at`, `updated_at`) VALUES
(1, 0, 'LINHA AMADEIRADOS', '2015-11-10 20:12:19', '2015-11-10 21:14:24'),
(2, 1, 'LINHA COLORS', '2015-11-10 21:07:22', '2015-11-10 21:14:35');

-- --------------------------------------------------------

--
-- Estrutura para tabela `linhas_imagens`
--

CREATE TABLE IF NOT EXISTS `linhas_imagens` (
`id` int(10) unsigned NOT NULL,
  `linha_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `linhas_imagens`
--

INSERT INTO `linhas_imagens` (`id`, `linha_id`, `ordem`, `imagem`, `produto`, `legenda`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '20151110181248_2.a-banco.JPG', 'Banco - dim.: 1,20 x 0,45 x 0,40', 'produto em MDF, acabamento em fórmica na cor carvalho  americano.', '2015-11-10 20:12:52', '2015-12-22 18:34:28'),
(3, 1, 9, '20151110181605_DSC_7312.JPG', 'Rack -  dim.: 1,00 x 0,56 x 0,46', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-11-10 20:16:12', '2015-12-22 18:36:47'),
(8, 1, 8, '20151110184708_IMG_1375.jpg', 'Cachepot - dim.: 0,45 x 0,45 x 0,45', 'produto em MDF, acabamento em fórmica na cor carvalho  americano.', '2015-11-10 20:47:15', '2015-12-22 18:30:36'),
(13, 1, 7, '20151110184824_DSC_7301.JPG', 'Mesa -  dim.: 1,40 x 0,74 x 0,70', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-11-10 20:48:31', '2015-12-22 18:33:34'),
(15, 1, 10, '20151110184846_DSC_7122.JPG', 'Nicho - dim.: 0,90 x 0,40 x 0,60', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-11-10 20:48:55', '2015-12-22 18:32:45'),
(21, 2, 6, '20151110190740_DSC_7311.jpg', 'Rack Café - dim.: 1,00 x 0,56 x 0,46', 'produto em MDF, acabamento em fórmica na cor Café.', '2015-11-10 21:07:43', '2015-12-22 18:46:13'),
(23, 2, 0, '20151110191035_BAÚ-MATRIZ-limpo.jpg', 'Baú preto - dim.: 0,90 x 0,50 x 0,50 ', 'produto em MDF, acabamento em fórmica na cor Preto.', '2015-11-10 21:10:40', '2016-01-05 18:34:18'),
(25, 2, 9, '20151110191055_DSC_8915.JPG', 'Carrinho com 3G dim.: 0,42 x 0,61 x 0,47', 'produto em MDF, acabamento em fórmica na cor Vermelho Cardeal.', '2015-11-10 21:11:02', '2015-12-22 18:51:38'),
(27, 2, 1, '20151110191128_DSC_8885.JPG', 'Estante Vermelha 4N - dim.: 1,10 x 0,86 x 0,40', 'produto em MDF, acabamento em fórmica na cor Vermelho Cardeal.', '2015-11-10 21:11:35', '2015-12-22 18:44:44'),
(29, 2, 5, '20151110191231_DSC_7742.jpg', 'Nicho Novo Cromo Real -  dim.: 0,90 x 0,40 x 0,60', 'produto em MDF, acabamento em fórmica na cor Novo Cromo Real.', '2015-11-10 21:12:35', '2015-12-22 18:44:15'),
(30, 2, 4, '20151110191231_DSC_7731.jpg', 'Nicho Verde Bambu - dim.: 0,90 x 0,40 x 0,60', 'produto em MDF, acabamento em fórmica na cor Verde Bambu.', '2015-11-10 21:12:36', '2015-12-22 18:43:50'),
(31, 2, 2, '20151110191230_DSC_7758.jpg', 'Nichos', '', '2015-11-10 21:12:36', '2016-01-05 18:34:52'),
(34, 1, 6, '20151201143955_DSC_7267.jpg', 'Estante 4N - dim.: 1,10 x 0,86 x 0,40', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-12-01 16:39:59', '2015-12-22 18:29:58'),
(35, 1, 12, '20151201144106_photo-1.JPG', 'Armário - dim.: 0,80 x 1,87 x 0,55', 'produto em MDF, acabamento em fórmica na cor carvalho  americano.', '2015-12-01 16:41:09', '2015-12-22 18:43:04'),
(37, 1, 4, '20151222174230_DSC_7232.jpg', 'Rack -  dim.: 1,00 x 056 x 046', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-12-22 19:42:33', '2016-01-05 18:31:35'),
(38, 1, 11, '20151222174233_DSC_7277.jpg', 'Estante 3N dim.: 1,10x 1,35 x 0,40', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-12-22 19:42:34', '2016-01-05 18:32:54'),
(39, 1, 0, '20151222174358_DSC_7329.jpg', 'Cômoda dim.: 1,10 x 0,85 x 0,45', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-12-22 19:43:59', '2016-01-05 18:30:20'),
(40, 1, 3, '20151222174548_DSC_7148.jpg', 'Baú - dim.:0,90 x 0,50 x 0,50', 'produto em MDF, acabamento em fórmica na cor carvalho natural.', '2015-12-22 19:45:57', '2016-01-05 18:31:22'),
(41, 1, 2, '20151222174754_DSC_7264.jpg', 'Carrinho com 2G -  dim.: 0,42 x 0,61 x 0,47', 'produto em MDF, acabamento em fórmica na cor carvalho  americano.', '2015-12-22 19:48:01', '2016-01-05 18:31:01'),
(42, 1, 5, '20151222174808_DSC_7225.jpg', '', '', '2015-12-22 19:48:17', '2015-12-22 19:48:17'),
(43, 2, 11, '20151222180210_comoda-03.jpg', 'Cômoda dim.: 1,10 x 0,85 x 0,45', 'produto em MDF, acabamento em fórmica na cor azulcobalto', '2015-12-22 20:02:12', '2016-01-05 18:38:15'),
(45, 2, 8, '20151222180237_mesa-02.jpg', 'Mesa -  dim.: 1,40 x 0,74 x 0,70', 'produto em MDF, acabamento em fórmica na cor Preto.', '2015-12-22 20:02:42', '2016-01-05 18:36:30'),
(47, 2, 3, '20151222181958_DSC_7722.jpg', 'Nicho - dim.: 0,90 x 0,40 x 0,60', 'produto em MDF, acabamento em fórmica na cor purple.', '2015-12-22 20:20:02', '2016-01-05 18:35:59'),
(48, 2, 10, '20151222182238_DSC_7119.jpg', 'Nicho - dim.: 0,90 x 0,40 x 0,60', 'produto em MDF, acabamento em fórmica na cor Preto.', '2015-12-22 20:22:43', '2016-01-05 18:36:52');

-- --------------------------------------------------------

--
-- Estrutura para tabela `marcenaria`
--

CREATE TABLE IF NOT EXISTS `marcenaria` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `marcenaria`
--

INSERT INTO `marcenaria` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 8, 'estante', 'estante', '2015-11-11 15:29:08', '2015-12-16 01:12:13'),
(2, 1, 'armário', 'armario', '2015-11-11 15:29:13', '2015-12-16 01:12:07'),
(3, 11, 'mesa de jantar', 'mesa-de-jantar', '2015-12-16 01:11:58', '2015-12-16 01:11:58'),
(4, 15, 'porta de correr', 'porta-de-correr', '2015-12-16 01:12:56', '2015-12-16 01:12:56'),
(5, 7, 'divisória', 'divisoria', '2015-12-16 01:13:11', '2015-12-16 01:13:11'),
(6, 6, 'cristaleira', 'cristaleira', '2015-12-16 01:15:03', '2015-12-16 01:15:03'),
(8, 0, 'nicho', 'nicho', '2015-12-16 01:15:41', '2015-12-22 19:17:18'),
(9, 10, 'mesa de escritório', 'mesa-de-escritorio', '2015-12-16 01:16:51', '2015-12-16 01:16:51'),
(10, 4, 'banco', 'banco', '2015-12-16 01:17:12', '2015-12-16 01:17:12'),
(11, 14, 'piso de madeira', 'piso-de-madeira', '2015-12-16 01:18:19', '2015-12-16 01:18:19'),
(12, 9, 'gaveteiro', 'gaveteiro', '2015-12-16 01:20:12', '2015-12-16 01:20:12'),
(13, 2, 'armário de cozinha', 'armario-de-cozinha', '2015-12-16 01:20:26', '2015-12-16 01:20:26'),
(14, 12, 'painel / revestimento', 'painel-revestimento', '2015-12-16 01:25:00', '2015-12-16 01:25:00'),
(15, 3, 'bancada', 'bancada', '2015-12-16 01:27:52', '2015-12-16 01:27:52'),
(16, 16, 'rack', 'rack', '2015-12-16 01:35:32', '2015-12-16 01:35:32'),
(17, 5, 'cabeceira', 'cabeceira', '2015-12-16 01:39:22', '2015-12-16 01:39:22'),
(18, 13, 'cômoda ', 'comoda', '2015-12-16 01:45:06', '2015-12-22 19:17:30');

-- --------------------------------------------------------

--
-- Estrutura para tabela `marcenaria_categoria_imagem`
--

CREATE TABLE IF NOT EXISTS `marcenaria_categoria_imagem` (
`id` int(10) unsigned NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  `imagem_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `marcenaria_categoria_imagem`
--

INSERT INTO `marcenaria_categoria_imagem` (`id`, `categoria_id`, `imagem_id`, `created_at`, `updated_at`) VALUES
(3, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 3, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 5, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 11, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 10, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 3, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 14, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 14, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 5, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 14, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 2, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 15, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 4, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 2, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 15, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 14, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 4, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 2, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 13, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 15, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 8, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 11, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 8, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 11, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 14, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 16, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 8, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 2, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 13, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 15, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 4, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 2, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 17, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 6, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 4, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 2, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 13, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 15, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 2, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 17, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 14, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 15, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 18, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 2, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 12, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 2, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 2, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 14, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 8, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 1, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 2, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 15, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 9, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 5, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 9, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 14, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 4, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 16, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 5, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 9, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 4, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 15, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 14, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 4, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 5, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 9, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 4, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `marcenaria_imagens`
--

CREATE TABLE IF NOT EXISTS `marcenaria_imagens` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `marcenaria_imagens`
--

INSERT INTO `marcenaria_imagens` (`id`, `ordem`, `imagem`, `legenda`, `created_at`, `updated_at`) VALUES
(2, 0, '20151215230542_DSC_7774_OK.jpg', '', '2015-12-16 01:05:48', '2015-12-16 01:05:48'),
(3, 0, '20151215231136_DSC_7773_OK.jpg', '', '2015-12-16 01:11:38', '2015-12-16 01:11:38'),
(4, 0, '20151215231804_DSC_7955.jpg', '', '2015-12-16 01:18:05', '2015-12-16 01:18:05'),
(5, 0, '20151215231858_DSC_7967.jpg', '', '2015-12-16 01:18:59', '2015-12-16 01:18:59'),
(6, 0, '20151215231917_DSC_7969_OK.jpg', '', '2015-12-16 01:19:18', '2015-12-16 01:19:18'),
(7, 0, '20151215232426_DSC_7990.jpg', '', '2015-12-16 01:24:27', '2015-12-16 01:24:27'),
(8, 0, '20151215232442_DSC_7999.jpg', '', '2015-12-16 01:24:43', '2015-12-16 01:24:43'),
(9, 0, '20151215232559_DSC_8051.jpg', '', '2015-12-16 01:26:01', '2015-12-16 01:26:01'),
(10, 0, '20151215232629_DSC_8054.jpg', '', '2015-12-16 01:26:30', '2015-12-16 01:26:30'),
(11, 0, '20151215232712_DSC_8057.jpg', '', '2015-12-16 01:27:14', '2015-12-16 01:27:14'),
(12, 0, '20151215232834_13-Biombo.jpg', '', '2015-12-16 01:28:38', '2015-12-16 01:28:38'),
(13, 0, '20151215232908_11-det-pia-banho.jpg', '', '2015-12-16 01:29:12', '2015-12-16 01:29:12'),
(14, 0, '20151215233010_10-vista.jpg', '', '2015-12-16 01:30:15', '2015-12-16 01:30:15'),
(15, 0, '20151215233147_1-cozinha.jpg', '', '2015-12-16 01:31:51', '2015-12-16 01:31:51'),
(16, 0, '20151215233329_2-cozinha.jpg', '', '2015-12-16 01:33:29', '2015-12-16 01:33:29'),
(17, 0, '20151215233807_3-sala-de-estar.jpg', '', '2015-12-16 01:38:08', '2015-12-16 01:38:08'),
(18, 0, '20151215233853_5-deck-e-portas-de-correr.jpg', '', '2015-12-16 01:38:53', '2015-12-16 01:38:53'),
(19, 0, '20151215233942_7-suite-casal.jpg', '', '2015-12-16 01:39:44', '2015-12-16 01:39:44'),
(20, 0, '20151215234254_COZINHA-1.jpg', '', '2015-12-16 01:42:55', '2015-12-16 01:42:55'),
(21, 0, '20151215234308_COZINHA.jpg', '', '2015-12-16 01:43:08', '2015-12-16 01:43:08'),
(22, 0, '20151215234329_DORM-CASAL.jpg', '', '2015-12-16 01:43:29', '2015-12-16 01:43:29'),
(23, 0, '20151215234753_AAA_5014.JPG', '', '2015-12-16 01:47:53', '2015-12-16 01:47:53'),
(24, 0, '20151215234939_AAA_2647.jpg', '', '2015-12-16 01:49:40', '2015-12-16 01:49:40'),
(25, 0, '20151215234954_AAA_2993.jpg', '', '2015-12-16 01:49:55', '2015-12-16 01:49:55'),
(26, 0, '20151215235025_AAA_2997.JPG', '', '2015-12-16 01:50:26', '2015-12-16 01:50:26'),
(27, 0, '20151215235142_AAA_3003.JPG', '', '2015-12-16 01:51:43', '2015-12-16 01:51:43'),
(28, 0, '20151215235220_AAA_3006.JPG', '', '2015-12-16 01:52:21', '2015-12-16 01:52:21'),
(29, 0, '20151215235248_AAA_3015.JPG', '', '2015-12-16 01:52:49', '2015-12-16 01:52:49'),
(30, 0, '20151215235334_AAA_3022.JPG', '', '2015-12-16 01:53:36', '2015-12-16 01:53:36'),
(31, 0, '20151215235409_AAA_3032.JPG', '', '2015-12-16 01:54:10', '2015-12-16 01:54:10'),
(32, 0, '20151215235435_AAA_3038.JPG', '', '2015-12-16 01:54:36', '2015-12-16 01:54:36');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_27_150209_create_banners_table', 1),
('2015_10_27_165217_create_contato_table', 1),
('2015_10_27_165245_create_contatos_recebidos_table', 1),
('2015_10_27_170340_create_blog_categorias_table', 1),
('2015_10_27_170349_create_blog_posts_table', 1),
('2015_10_27_170358_create_blog_comentarios_table', 1),
('2015_10_27_170939_create_projetos_table', 1),
('2015_10_27_170953_create_projetos_imagens_table', 1),
('2015_10_27_171027_create_marcenaria_table', 1),
('2015_10_27_171038_create_marcenaria_imagens_table', 1),
('2015_10_27_171059_create_linhas_table', 1),
('2015_10_27_171111_create_linhas_imagens_table', 1),
('2015_10_27_171135_create_produtos_categorias_table', 1),
('2015_10_27_171145_create_produtos_table', 1),
('2015_10_27_171242_create_produtos_imagens_table', 1),
('2015_10_27_172535_create_quem_somos_table', 1),
('2015_10_27_172553_create_parceiros_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `parceiros`
--

CREATE TABLE IF NOT EXISTS `parceiros` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
`id` int(10) unsigned NOT NULL,
  `produtos_categoria_id` int(10) unsigned DEFAULT NULL,
  `linha_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `linha_id`, `ordem`, `titulo`, `slug`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 0, 'Cômoda Amadeirada', 'comoda-amadeirada', 'Teste descrição.', '2015-11-10 21:18:48', '2015-12-21 18:46:59'),
(2, 4, 2, 0, 'Cômoda Color', 'comoda-color', 'teste descrição.', '2015-11-10 21:19:29', '2015-12-21 18:46:17'),
(3, 2, 1, 0, 'Baú Amadeirado', 'bau-amadeirado', 'teste descrição com \r\nmais de uma\r\nlinha', '2015-11-10 21:20:21', '2015-12-21 18:44:44'),
(4, 2, 2, 0, 'Baú Colors', 'bau-colors', 'teste\r\ndescrição\r\nmuitas\r\nlinhas\r\npara\r\ndescrição\r\ndo produto.', '2015-11-10 21:21:22', '2015-11-10 21:21:22'),
(5, 1, 2, 0, 'Nicho Colors', 'nicho-colors', 'teste', '2015-11-10 21:22:22', '2015-11-10 21:22:22'),
(6, 8, 1, 0, 'Rack Amadeirado', 'rack-amadeirado', 'teste', '2015-11-10 21:24:33', '2015-11-10 21:24:57'),
(7, 6, 1, 0, 'Cachepot Amadeirado', 'cachepot-amadeirado', 'teste', '2015-11-10 21:25:32', '2015-11-10 21:25:32'),
(8, 5, 1, 0, 'Bar Amadeirado', 'bar-amadeirado', 'teste', '2015-11-10 21:26:12', '2015-11-10 21:26:12'),
(9, 7, 1, 0, 'Carrinho Amadeirado', 'carrinho-amadeirado', 'teste', '2015-11-10 21:26:37', '2015-11-10 21:26:37'),
(11, 9, 2, 0, 'Mesa Colors', 'mesa-colors', 'teste', '2015-11-10 21:28:28', '2015-11-10 21:28:28'),
(12, 1, 1, 0, 'Nicho Volante Amadeirado', 'nicho-volante-amadeirado', 'Nicho volante amadeirado', '2015-12-16 19:13:10', '2015-12-22 20:44:53'),
(14, 3, 1, 0, 'Estante', 'estante', 'Estante', '2015-12-16 19:48:12', '2015-12-16 19:48:12'),
(15, 12, 1, 0, 'Armário', 'armario', 'Armário amadeirado.', '2015-12-16 20:01:22', '2015-12-16 20:01:22'),
(16, 3, 1, 0, 'Estante baixa', 'estante-baixa', 'Estante com 4 nichos.', '2015-12-16 20:03:43', '2015-12-22 20:42:03'),
(17, 9, 1, 0, 'Mesa amadeirada', 'mesa-amadeirada', 'Mesa amadeirada', '2015-12-16 20:05:40', '2015-12-16 20:05:40'),
(18, 3, 2, 0, 'Estante baixa colors', 'estante-baixa-colors', 'Estante baixa Colors', '2015-12-16 20:31:52', '2015-12-16 20:31:52'),
(19, 5, 2, 0, 'Bar Colors', 'bar-colors', 'Bar Colors', '2015-12-16 20:34:14', '2015-12-16 20:34:14');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos_categorias`
--

CREATE TABLE IF NOT EXISTS `produtos_categorias` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 6, 'nicho volante', 'nicho-volante', '2015-11-10 21:15:02', '2015-11-10 21:17:32'),
(2, 1, 'baú', 'bau', '2015-11-10 21:15:10', '2015-11-10 21:17:26'),
(3, 5, 'estante', 'estante', '2015-11-10 21:15:15', '2015-11-10 21:17:19'),
(4, 4, 'cômoda', 'comoda', '2015-11-10 21:15:21', '2015-11-10 21:17:13'),
(5, 0, 'bar', 'bar', '2015-11-10 21:15:38', '2015-11-10 21:15:38'),
(6, 2, 'cachepot', 'cachepot', '2015-11-10 21:15:44', '2015-11-10 21:17:06'),
(7, 3, 'carrinho', 'carrinho', '2015-11-10 21:15:49', '2015-11-10 21:17:00'),
(8, 8, 'rack', 'rack', '2015-11-10 21:16:07', '2015-11-10 21:16:49'),
(9, 7, 'mesa', 'mesa', '2015-11-10 21:17:55', '2015-11-10 21:17:55'),
(12, 0, 'armário', 'armario', '2015-12-16 19:58:53', '2015-12-16 19:58:53');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos_imagens`
--

CREATE TABLE IF NOT EXISTS `produtos_imagens` (
`id` int(10) unsigned NOT NULL,
  `produto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos_imagens`
--

INSERT INTO `produtos_imagens` (`id`, `produto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 2, 0, '20151110191948_Comoda-Colors-perfilado-natural.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(4, 2, 0, '20151110191948_comoda-vermelha.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(5, 2, 0, '20151110191948_comoda-verde-bambu.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(6, 2, 0, '20151110191948_CÔMODA-MATRIZ.jpg', '2015-11-10 21:19:50', '2015-11-10 21:19:50'),
(10, 5, 0, '20151110192404_nicho-quadrado-amarelo.jpg', '2015-11-10 21:24:05', '2015-11-10 21:24:05'),
(11, 5, 0, '20151110192404_nicho-quadrado-vermelho.jpg', '2015-11-10 21:24:05', '2015-11-10 21:24:05'),
(13, 6, 0, '20151110192510_rack-01.png', '2015-11-10 21:25:13', '2015-11-10 21:25:13'),
(34, 1, 0, '20151216170549_7.a-Comoda---edit.png', '2015-12-16 19:05:51', '2015-12-16 19:05:51'),
(35, 1, 0, '20151216170557_7.b-comoda-edit.png', '2015-12-16 19:05:58', '2015-12-16 19:05:58'),
(36, 4, 0, '20151216170838_bau-IMG_2873.png', '2015-12-16 19:08:39', '2015-12-16 19:08:39'),
(38, 12, 0, '20151216171320_10.a-nicho-edit.png', '2015-12-16 19:13:21', '2015-12-16 19:13:21'),
(39, 9, 0, '20151216171402_6.b-Carrinho-3G-edit.png', '2015-12-16 19:14:05', '2015-12-16 19:14:05'),
(40, 9, 0, '20151216171532_6.a-Carrinho-2G-edit.png', '2015-12-16 19:15:34', '2015-12-16 19:15:34'),
(41, 6, 0, '20151216171924_11.a-Rack-aberto.jpg', '2015-12-16 19:19:25', '2015-12-16 19:19:25'),
(42, 7, 0, '20151216172205_5.A-Cachepot.png', '2015-12-16 19:22:07', '2015-12-16 19:22:07'),
(43, 3, 0, '20151216172332_4.a-Bau.png', '2015-12-16 19:23:34', '2015-12-16 19:23:34'),
(44, 3, 0, '20151216173654_bau-amadeirado-aberto.png', '2015-12-16 19:36:58', '2015-12-16 19:36:58'),
(45, 14, 0, '20151216174831_8.a-Estante-3N.png', '2015-12-16 19:48:40', '2015-12-16 19:48:40'),
(46, 5, 0, '20151216175306_Nicho-amarelo.png', '2015-12-16 19:53:07', '2015-12-16 19:53:07'),
(47, 8, 0, '20151216175744_3.a-Bar.png', '2015-12-16 19:57:47', '2015-12-16 19:57:47'),
(48, 15, 0, '20151216180135_1.a-Armario.png', '2015-12-16 20:01:36', '2015-12-16 20:01:36'),
(50, 15, 0, '20151216180158_1.b-Armario-02.png', '2015-12-16 20:02:07', '2015-12-16 20:02:07'),
(51, 16, 0, '20151216180358_8.b-Estante-4N.png', '2015-12-16 20:03:59', '2015-12-16 20:03:59'),
(52, 17, 0, '20151216180559_9.a-Mesa.png', '2015-12-16 20:06:02', '2015-12-16 20:06:02'),
(53, 11, 0, '20151216182443_mesa-vermelha.jpg', '2015-12-16 20:24:45', '2015-12-16 20:24:45'),
(54, 11, 0, '20151216182443_mesa-roxa.jpg', '2015-12-16 20:24:45', '2015-12-16 20:24:45'),
(55, 11, 0, '20151216182443_mesa-preta.jpg', '2015-12-16 20:24:45', '2015-12-16 20:24:45'),
(56, 11, 0, '20151216182443_mesa-verde.jpg', '2015-12-16 20:24:45', '2015-12-16 20:24:45'),
(57, 18, 0, '20151216183352_estante-baixa-vermelha.jpg', '2015-12-16 20:33:52', '2015-12-16 20:33:52'),
(58, 19, 0, '20151216183423_bar-amarelo.jpg', '2015-12-16 20:34:23', '2015-12-16 20:34:23'),
(59, 5, 0, '20151216183437_nicho-volante-roxo.jpg', '2015-12-16 20:34:38', '2015-12-16 20:34:38');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquiteto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `titulo`, `arquiteto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, 'Cobertura Vila Olímpia', 'Arq. Arildo de Andrade Azevedo', '20151110160120_5-deck-e-portas-de-correr.jpg', '2015-11-10 18:01:23', '2015-12-17 20:11:27'),
(2, 1, 'Jardim Europa', 'Arq. Helena Camargo', '20151110160409_DORM-CASAL.jpg', '2015-11-10 18:04:09', '2015-12-17 20:12:12'),
(3, 2, 'SOB MEDIDA', 'Arq. Candida Tabet', '20151110160623_DSC_7768_OK.jpg', '2015-11-10 18:06:27', '2015-12-17 20:12:56'),
(4, 3, 'MOBILIÁRIO CORPORATIVO', 'Arq. Arildo de Andrade Azevedo', '20151110171434_10-vista.jpg', '2015-11-10 19:14:34', '2015-12-17 20:23:40'),
(5, 0, 'MARCENARIA COM SERIEDADE', 'Arq. Otavio de Sanctis', '20151215225422_AAA_3038.JPG', '2015-12-16 00:54:23', '2016-01-05 19:47:37'),
(6, 0, 'Espaços integrados', 'Arq. Arildo de Andrade Azevedo', '20151215225650_AAA_2996.JPG', '2015-12-16 00:56:50', '2015-12-17 20:09:27'),
(7, 0, 'RODALU', 'Moda infanto-juvenil ', '20151215225901_AAA_5041.JPG', '2015-12-16 00:59:02', '2015-12-17 20:22:12'),
(8, 0, 'Marcenaria Imbatível ', 'Arq. Helena Camargo', '20151215230114_COZINHA.jpg', '2015-12-16 01:01:15', '2015-12-17 20:36:33');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos_imagens`
--

CREATE TABLE IF NOT EXISTS `projetos_imagens` (
`id` int(10) unsigned NOT NULL,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `legenda`, `created_at`, `updated_at`) VALUES
(1, 1, 7, '20151110160143_6-deck-spa.jpg', '', '2015-11-10 18:01:48', '2015-11-10 18:01:48'),
(2, 1, 4, '20151110160145_2-cozinha.jpg', '', '2015-11-10 18:01:49', '2015-11-10 18:01:49'),
(4, 1, 2, '20151110160150_4-sala-de-estar.jpg', '', '2015-11-10 18:01:54', '2015-11-10 18:01:54'),
(5, 1, 3, '20151110160153_1-cozinha.jpg', '', '2015-11-10 18:02:00', '2015-11-10 18:02:00'),
(6, 1, 5, '20151110160153_5-deck-e-portas-de-correr.jpg', '', '2015-11-10 18:02:00', '2015-11-10 18:02:00'),
(7, 1, 8, '20151110160156_7-suite-casal.jpg', '', '2015-11-10 18:02:00', '2015-11-10 18:02:00'),
(8, 1, 6, '20151110160157_8-vista-geral.jpg', '', '2015-11-10 18:02:02', '2015-11-10 18:02:02'),
(9, 1, 1, '20151110160159_9-vista-geral-II.jpg', '', '2015-11-10 18:02:05', '2015-11-10 18:02:05'),
(10, 2, 0, '20151110160420_135528_e5bbd0c75a5948568644e84f748112a5.jpg_srz_323_429_75_22_0.50_1.20_0.00_jpg_srz.jpg', '', '2015-11-10 18:04:20', '2015-11-10 18:04:20'),
(11, 2, 0, '20151110160421_COZINHA-1.jpg', '', '2015-11-10 18:04:21', '2015-11-10 18:04:21'),
(12, 2, 0, '20151110160421_DORM-CASAL.jpg', '', '2015-11-10 18:04:21', '2015-11-10 18:04:21'),
(13, 2, 0, '20151110160421_COZINHA.jpg', '', '2015-11-10 18:04:21', '2015-11-10 18:04:21'),
(14, 3, 1, '20151110164926_DSC_7864_OK.jpg', '', '2015-11-10 18:49:38', '2015-11-10 18:49:38'),
(15, 3, 0, '20151110164927_DSC_7796_OK.jpg', '', '2015-11-10 18:49:40', '2015-11-10 18:49:40'),
(16, 3, 25, '20151110164928_DSC_7773_OK.jpg', '', '2015-11-10 18:49:40', '2015-11-10 18:49:40'),
(17, 3, 2, '20151110164929_DSC_7768_OK.jpg', '', '2015-11-10 18:49:43', '2015-11-10 18:49:43'),
(18, 3, 22, '20151110164931_DSC_7784_OK.jpg', '', '2015-11-10 18:49:47', '2015-11-10 18:49:47'),
(19, 3, 11, '20151110164933_DSC_7774_OK.jpg', '', '2015-11-10 18:49:50', '2015-11-10 18:49:50'),
(21, 3, 23, '20151110165000_DSC_7919.jpg', '', '2015-11-10 18:50:10', '2015-11-10 18:50:10'),
(22, 3, 21, '20151110165000_DSC_7900.jpg', '', '2015-11-10 18:50:11', '2015-11-10 18:50:11'),
(23, 3, 10, '20151110165010_DSC_7942.jpg', '', '2015-11-10 18:50:21', '2015-11-10 18:50:21'),
(24, 3, 20, '20151110165011_DSC_7955.jpg', '', '2015-11-10 18:50:21', '2015-11-10 18:50:21'),
(25, 3, 19, '20151110165010_DSC_7911_OK.jpg', '', '2015-11-10 18:50:22', '2015-11-10 18:50:22'),
(26, 3, 18, '20151110165016_DSC_7959.jpg', '', '2015-11-10 18:50:27', '2015-11-10 18:50:27'),
(27, 3, 17, '20151110165022_DSC_7967.jpg', '', '2015-11-10 18:50:31', '2015-11-10 18:50:31'),
(28, 3, 16, '20151110165022_DSC_7969_OK.jpg', '', '2015-11-10 18:50:32', '2015-11-10 18:50:32'),
(29, 3, 13, '20151110165037_DSC_7990.jpg', '', '2015-11-10 18:50:45', '2015-11-10 18:50:45'),
(30, 3, 15, '20151110165040_DSC_7974.jpg', '', '2015-11-10 18:50:50', '2015-11-10 18:50:50'),
(31, 3, 14, '20151110165048_DSC_7998.jpg', '', '2015-11-10 18:51:01', '2015-11-10 18:51:01'),
(32, 3, 12, '20151110165048_DSC_7999.jpg', '', '2015-11-10 18:51:02', '2015-11-10 18:51:02'),
(34, 3, 8, '20151110165059_DSC_8011_OK.jpg', '', '2015-11-10 18:51:14', '2015-11-10 18:51:14'),
(35, 3, 7, '20151110165101_DSC_8049.jpg', '', '2015-11-10 18:51:15', '2015-11-10 18:51:15'),
(36, 3, 6, '20151110165103_DSC_8051.jpg', '', '2015-11-10 18:51:19', '2015-11-10 18:51:19'),
(37, 3, 5, '20151110165112_DSC_8054.jpg', '', '2015-11-10 18:51:24', '2015-11-10 18:51:24'),
(38, 3, 4, '20151110165113_DSC_8057.jpg', '', '2015-11-10 18:51:25', '2015-11-10 18:51:25'),
(39, 3, 3, '20151110165117_DSC_8084.jpg', '', '2015-11-10 18:51:27', '2015-11-10 18:51:27'),
(40, 4, 3, '20151110171445_10-vista.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(41, 4, 2, '20151110171445_6-sala-sócios.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(42, 4, 6, '20151110171445_11-det-pia-banho.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(43, 4, 1, '20151110171445_8-estante.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(44, 4, 5, '20151110171445_12-vista-biombo.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(45, 4, 0, '20151110171445_1-staff.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(46, 4, 4, '20151110171447_13-Biombo.jpg', '', '2015-11-10 19:14:49', '2015-11-10 19:14:49'),
(47, 5, 6, '20151215225434_AAA_3003.JPG', '', '2015-12-16 00:54:35', '2015-12-16 00:54:35'),
(48, 5, 5, '20151215225437_AAA_3005.JPG', '', '2015-12-16 00:54:38', '2015-12-16 00:54:38'),
(49, 5, 4, '20151215225438_AAA_3015.JPG', '', '2015-12-16 00:54:40', '2015-12-16 00:54:40'),
(50, 5, 3, '20151215225439_AAA_3038.JPG', '', '2015-12-16 00:54:40', '2015-12-16 00:54:40'),
(51, 5, 0, '20151215225439_AAA_3032.JPG', '', '2015-12-16 00:54:41', '2015-12-16 00:54:41'),
(52, 5, 2, '20151215225440_AAA_3006.JPG', '', '2015-12-16 00:54:41', '2015-12-16 00:54:41'),
(53, 5, 1, '20151215225440_AAA_3022.JPG', '', '2015-12-16 00:54:41', '2015-12-16 00:54:41'),
(54, 6, 2, '20151215225658_AAA_2647.jpg', '', '2015-12-16 00:56:59', '2015-12-16 00:56:59'),
(56, 6, 0, '20151215225701_AAA_2997.JPG', '', '2015-12-16 00:57:02', '2015-12-16 00:57:02'),
(57, 6, 1, '20151215225702_AAA_2993.jpg', '', '2015-12-16 00:57:03', '2015-12-16 00:57:03'),
(59, 7, 4, '20151215225915_AAA_5011.JPG', '', '2015-12-16 00:59:16', '2015-12-16 00:59:16'),
(60, 7, 3, '20151215225915_AAA_5014.JPG', '', '2015-12-16 00:59:16', '2015-12-16 00:59:16'),
(61, 7, 2, '20151215225916_AAA_5015.JPG', '', '2015-12-16 00:59:17', '2015-12-16 00:59:17'),
(62, 7, 5, '20151215225916_AAA_5041.JPG', '', '2015-12-16 00:59:17', '2015-12-16 00:59:17'),
(63, 7, 0, '20151215225917_AAA_5028.JPG', '', '2015-12-16 00:59:18', '2015-12-16 00:59:18'),
(64, 8, 0, '20151215230123_135528_e5bbd0c75a5948568644e84f748112a5.jpg_srz_323_429_75_22_0.50_1.20_0.00_jpg_srz.jpg', '', '2015-12-16 01:01:24', '2015-12-17 20:35:53'),
(65, 8, 3, '20151215230123_DORM-CASAL.jpg', '', '2015-12-16 01:01:24', '2015-12-17 20:36:09'),
(66, 8, 2, '20151215230124_COZINHA-1.jpg', '', '2015-12-16 01:01:24', '2015-12-17 20:36:07'),
(67, 8, 1, '20151215230124_COZINHA.jpg', '', '2015-12-16 01:01:25', '2015-12-17 20:36:02');

-- --------------------------------------------------------

--
-- Estrutura para tabela `quem_somos`
--

CREATE TABLE IF NOT EXISTS `quem_somos` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>O projeto da Girante resultou de vinte cinco anos&nbsp;de experi&ecirc;ncia&nbsp;do arquiteto e paisagista Arildo de Andrade Azevedo, especializado&nbsp;no desenvolvimento e na execu&ccedil;&atilde;o de projetos do ramo mobili&aacute;rio,&nbsp;para fornecimento a clientes e parceiros interessados em adquirir&nbsp;pe&ccedil;as prontas, fabricadas com a qualidade e a terminologia&nbsp;dos&nbsp;produtos cuidadosamente planejados.</p>\r\n\r\n<p>Para sua concep&ccedil;&atilde;o, o arquiteto criou uma linha de m&oacute;veis com&nbsp;rod&iacute;zios, que despertou o interesse de in&uacute;meros industriais,&nbsp;comerciantes e profissionais.</p>\r\n\r\n<p>Gra&ccedil;as as suas praticidade e&nbsp;durabilidade, esses artefatos s&atilde;o de&nbsp;f&aacute;cil deslocamento para limpeza e manuten&ccedil;&atilde;o, qualidades desejadas&nbsp;por compradores no seu dia&nbsp;a&nbsp;dia.</p>\r\n\r\n<p>Outra vantagem dos artigos assim planejados est&aacute; na possibilidade&nbsp;da f&aacute;cil adapta&ccedil;&atilde;o de medidas, cores e tipos de acabamento e textura.</p>\r\n', '20160106171134_printvideo-2.jpg', '0000-00-00 00:00:00', '2016-01-07 14:17:54');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$i4QWv9joIh1EFj5Cwypb7.gMBFNk8h3iIyn.aBnT5rvQ9XOtKFZSe', 'tf0HVgG4ptOLuUe690MMRXuYdsMfG7X7Db3FdXaQl8M3cM5QIHTg6jxl6lai', '0000-00-00 00:00:00', '2015-11-09 20:29:18'),
(2, 'girante', 'girante@trupe.net', '$2y$10$7WeP5S2k2/VIaA/fGU8XA.va7g/2oZtQLiJMVew/Y1o51xdnkfUS2', 'kBOipFhjRYyiLtywDb4EpH9cG28p1UMC7RIf4MgFug7fMabZ38LxRwAnmrZv', '2015-11-10 17:57:36', '2015-11-24 15:31:10');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `blog_categorias`
--
ALTER TABLE `blog_categorias`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `blog_comentarios`
--
ALTER TABLE `blog_comentarios`
 ADD PRIMARY KEY (`id`), ADD KEY `blog_comentarios_blog_post_id_foreign` (`blog_post_id`);

--
-- Índices de tabela `blog_posts`
--
ALTER TABLE `blog_posts`
 ADD PRIMARY KEY (`id`), ADD KEY `blog_posts_blog_categoria_id_foreign` (`blog_categoria_id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `linhas`
--
ALTER TABLE `linhas`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `linhas_imagens`
--
ALTER TABLE `linhas_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `linhas_imagens_linha_id_foreign` (`linha_id`);

--
-- Índices de tabela `marcenaria`
--
ALTER TABLE `marcenaria`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `marcenaria_categoria_imagem`
--
ALTER TABLE `marcenaria_categoria_imagem`
 ADD PRIMARY KEY (`id`), ADD KEY `marcenaria_categoria_imagem_categoria_id_index` (`categoria_id`), ADD KEY `marcenaria_categoria_imagem_imagem_id_index` (`imagem_id`);

--
-- Índices de tabela `marcenaria_imagens`
--
ALTER TABLE `marcenaria_imagens`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `parceiros`
--
ALTER TABLE `parceiros`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
 ADD PRIMARY KEY (`id`), ADD KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`), ADD KEY `produtos_linha_id_foreign` (`linha_id`);

--
-- Índices de tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `produtos_imagens_produto_id_foreign` (`produto_id`);

--
-- Índices de tabela `projetos`
--
ALTER TABLE `projetos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Índices de tabela `quem_somos`
--
ALTER TABLE `quem_somos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `blog_categorias`
--
ALTER TABLE `blog_categorias`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `blog_comentarios`
--
ALTER TABLE `blog_comentarios`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `blog_posts`
--
ALTER TABLE `blog_posts`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `linhas`
--
ALTER TABLE `linhas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `linhas_imagens`
--
ALTER TABLE `linhas_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de tabela `marcenaria`
--
ALTER TABLE `marcenaria`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de tabela `marcenaria_categoria_imagem`
--
ALTER TABLE `marcenaria_categoria_imagem`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT de tabela `marcenaria_imagens`
--
ALTER TABLE `marcenaria_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de tabela `parceiros`
--
ALTER TABLE `parceiros`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de tabela `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de tabela `projetos`
--
ALTER TABLE `projetos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT de tabela `quem_somos`
--
ALTER TABLE `quem_somos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `blog_comentarios`
--
ALTER TABLE `blog_comentarios`
ADD CONSTRAINT `blog_comentarios_blog_post_id_foreign` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `blog_posts`
--
ALTER TABLE `blog_posts`
ADD CONSTRAINT `blog_posts_blog_categoria_id_foreign` FOREIGN KEY (`blog_categoria_id`) REFERENCES `blog_categorias` (`id`) ON DELETE SET NULL;

--
-- Restrições para tabelas `linhas_imagens`
--
ALTER TABLE `linhas_imagens`
ADD CONSTRAINT `linhas_imagens_linha_id_foreign` FOREIGN KEY (`linha_id`) REFERENCES `linhas` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `marcenaria_categoria_imagem`
--
ALTER TABLE `marcenaria_categoria_imagem`
ADD CONSTRAINT `marcenaria_categoria_imagem_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `marcenaria` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `marcenaria_categoria_imagem_imagem_id_foreign` FOREIGN KEY (`imagem_id`) REFERENCES `marcenaria_imagens` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `produtos`
--
ALTER TABLE `produtos`
ADD CONSTRAINT `produtos_linha_id_foreign` FOREIGN KEY (`linha_id`) REFERENCES `linhas` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

--
-- Restrições para tabelas `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
ADD CONSTRAINT `produtos_imagens_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
