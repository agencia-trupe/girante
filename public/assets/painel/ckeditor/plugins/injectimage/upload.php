<?php

// example method
public function imageUpload(Request $request)
{
    $validator = \Validator::make($request->all(), [
        'imagem' => 'image|required'
    ]);

    if ($validator->fails()) {
        $response = [
            'error'   => true,
            'message' => 'O arquivo deve ser uma imagem.'
        ];
    } else {
        $imagem   = CropImage::make('imagem', $this->image_config);
        $response = [
            'filepath' => asset($this->image_config['path'] . $imagem)
        ];
    }

    return response()->json($response);
}
