-- phpMyAdmin SQL Dump
-- version 4.4.14.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2015 at 01:11 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.6.10-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `girante`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `frase`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, '20151109183037_img-home1.jpg', 'Marcenaria Personalizada', 'executamos projetos · criação própria ou de outros escritórios', 'http://www.trupe.net/previa-girante/marcenaria-personalizada', '2015-11-09 20:30:43', '2015-11-09 20:30:43'),
(2, 1, '20151109183202_img-home2.jpg', 'Mobiliário sob medida', 'projetos de móveis customizados para cada necessidade · excelente acabamento', 'http://www.trupe.net/previa-girante/marcenaria-personalizada', '2015-11-09 20:32:05', '2015-11-09 20:32:05'),
(3, 2, '20151109183252_img-home3.jpg', 'Linha de móveis estilo industrial', 'à pronta entrega ou sob medida · alta resistência e durabilidade em um estilo singular', 'http://www.trupe.net/previa-girante/linha-girante', '2015-11-09 20:32:55', '2015-11-09 20:32:55');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categorias`
--

CREATE TABLE IF NOT EXISTS `blog_categorias` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_categorias`
--

INSERT INTO `blog_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Girante na mídia', 'girante-na-midia', '2015-11-10 21:31:20', '2015-11-10 21:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comentarios`
--

CREATE TABLE IF NOT EXISTS `blog_comentarios` (
  `id` int(10) unsigned NOT NULL,
  `blog_post_id` int(10) unsigned NOT NULL,
  `autor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` text COLLATE utf8_unicode_ci NOT NULL,
  `aprovado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(10) unsigned NOT NULL,
  `blog_categoria_id` int(10) unsigned DEFAULT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `blog_categoria_id`, `data`, `titulo`, `slug`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, '2013-11-10', 'Marcenaria imbatível', 'marcenaria-imbativel', '20151110193903_135528_e5bbd0c75a5948568644e84f748112a5.jpg_srz_323_429_75_22_0.50_1.20_0.00_jpg_srz.jpg', '<p>Projeto executado pela Girante &eacute; capa da revista Casa e Jardim.</p>\r\n\r\n<p>O projeto de [nome arquiteto] foi todo produzido pela Girante Marcenaria e encantou os editores da revista Casa e Jardim que atribu&iacute;ram um t&iacute;tulo que resume o trabalho da Girante: uma MARCENARIA IMBAT&Iacute;VEL.</p>\r\n\r\n<p>O apartamento contou com cozinha,&nbsp;estar e quarto&nbsp;completos com acabamento em [nome da madeira e materiais].</p>\r\n\r\n<p>Confira mais algumas imagens do projeto:</p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110193831_COZINHA.jpg" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110193850_COZINHA-1.jpg" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110193900_DORM-CASAL.jpg" /></p>\r\n', '2015-11-10 21:39:03', '2015-11-10 21:39:03'),
(2, 1, '2015-07-10', 'Móveis da Girante no programa "Olho Mágico - Reforma de Vizinhos" no GNT', 'moveis-da-girante-no-programa-olho-magico-reforma-de-vizinhos-no-gnt', '20151110195101_reforma-de-vizinhos.png', '<p>Os M&oacute;veis da Linha Girante ajudaram a compor a decora&ccedil;&atilde;o criativa da arquiteta [nome arquiteta] para o programa do GNT &quot;Olho M&aacute;gico -&nbsp;Reforma de Vizinhos&quot;.</p>\r\n\r\n<p>O epis&oacute;dio foi ao ar dia 29 de abril 2015 e pode ser visto completo no site do GNT no link:&nbsp;<a href="http://gnt.globo.com/programas/olho-magico-reforma-de-vizinhos/videos/4212972.htm" target="_blank">http://gnt.globo.com/programas/olho-magico-reforma-de-vizinhos/videos/4212972.htm</a></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200712_reforma-de-vizinhos.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200722_reforma-de-vizinhos2.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200732_reforma-de-vizinhos3.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200743_reforma-de-vizinhos4.png" /></p>\r\n\r\n<p><img src="http://www.trupe.net/previa-girante/assets/img/blog/imagens/20151110200752_reforma-de-vizinhos5.png" /></p>\r\n', '2015-11-10 21:51:01', '2015-11-10 22:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contato`
--

INSERT INTO `contato` (`id`, `telefone`, `email`, `endereco`, `googlemaps`, `facebook`, `created_at`, `updated_at`) VALUES
(1, '+55 11 2738·4166', 'contato@girante.com.br', '<p>Rua Romilda Margarida Gabriel, 162</p><p>Itaim Bibi - São Paulo - SP</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.7475932903526!2d-46.674049848740204!3d-23.577507084599663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce576049fb239f%3A0x33faf736613cacd8!2sR.+Romilda+Margarida+Gabriel%2C+162+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04530-090!5e0!3m2!1spt-BR!2sbr!4v1446036605342" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://www.facebook.com/girantemoveis', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `linhas`
--

CREATE TABLE IF NOT EXISTS `linhas` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `linhas`
--

INSERT INTO `linhas` (`id`, `ordem`, `titulo`, `created_at`, `updated_at`) VALUES
(1, 0, 'LINHA AMADEIRADOS', '2015-11-10 20:12:19', '2015-11-10 21:14:24'),
(2, 1, 'LINHA COLORS', '2015-11-10 21:07:22', '2015-11-10 21:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `linhas_imagens`
--

CREATE TABLE IF NOT EXISTS `linhas_imagens` (
  `id` int(10) unsigned NOT NULL,
  `linha_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `linhas_imagens`
--

INSERT INTO `linhas_imagens` (`id`, `linha_id`, `ordem`, `imagem`, `produto`, `legenda`, `created_at`, `updated_at`) VALUES
(1, 1, 12, '20151110181248_2.a-banco.JPG', '', '', '2015-11-10 20:12:52', '2015-11-10 20:12:52'),
(2, 1, 19, '20151110181604_DSC_7308.JPG', '', '', '2015-11-10 20:16:12', '2015-11-10 20:16:12'),
(3, 1, 18, '20151110181605_DSC_7312.JPG', '', '', '2015-11-10 20:16:12', '2015-11-10 20:16:12'),
(4, 1, 17, '20151110184557_IMG_0031.JPG', '', '', '2015-11-10 20:46:03', '2015-11-10 20:46:03'),
(5, 1, 4, '20151110184558_DSC_8925.JPG', '', '', '2015-11-10 20:46:08', '2015-11-10 20:46:08'),
(6, 1, 11, '20151110184601_DSC_8924.JPG', '', '', '2015-11-10 20:46:10', '2015-11-10 20:46:10'),
(7, 1, 15, '20151110184708_IMG_1434.jpg', '', '', '2015-11-10 20:47:14', '2015-11-10 20:47:14'),
(8, 1, 1, '20151110184708_IMG_1375.jpg', '', '', '2015-11-10 20:47:15', '2015-11-10 20:47:15'),
(9, 1, 14, '20151110184726_Cópia-de-GAVETEIRO-2N.jpg', '', '', '2015-11-10 20:47:29', '2015-11-10 20:47:29'),
(10, 1, 3, '20151110184726_DSC_7254.jpg', '', '', '2015-11-10 20:47:29', '2015-11-10 20:47:29'),
(11, 1, 2, '20151110184754_DSC_7322-2.jpg', '', '', '2015-11-10 20:48:07', '2015-11-10 20:48:07'),
(12, 1, 13, '20151110184755_DSC_7329.JPG', '', '', '2015-11-10 20:48:08', '2015-11-10 20:48:08'),
(13, 1, 10, '20151110184824_DSC_7301.JPG', '', '', '2015-11-10 20:48:31', '2015-11-10 20:48:31'),
(14, 1, 9, '20151110184846_DSC_7123-2.jpg', '', '', '2015-11-10 20:48:54', '2015-11-10 20:48:54'),
(15, 1, 8, '20151110184846_DSC_7122.JPG', '', '', '2015-11-10 20:48:55', '2015-11-10 20:48:55'),
(16, 1, 7, '20151110185033_IMG_0036.JPG', '', '', '2015-11-10 20:50:44', '2015-11-10 20:50:44'),
(17, 1, 6, '20151110185035_IMG_0038.JPG', '', '', '2015-11-10 20:50:46', '2015-11-10 20:50:46'),
(18, 1, 5, '20151110185038_DSC_8979.JPG', '', '', '2015-11-10 20:50:52', '2015-11-10 20:50:52'),
(19, 1, 16, '20151110185038_DSC_8980.JPG', '', '', '2015-11-10 20:50:52', '2015-11-10 20:50:52'),
(20, 1, 0, '20151110185037_DSC_8982.JPG', '', '', '2015-11-10 20:50:52', '2015-11-10 20:50:52'),
(21, 2, 0, '20151110190740_DSC_7311.jpg', '', '', '2015-11-10 21:07:43', '2015-11-10 21:07:43'),
(23, 2, 0, '20151110191035_BAÚ-MATRIZ-limpo.jpg', '', '', '2015-11-10 21:10:40', '2015-11-10 21:10:40'),
(24, 2, 0, '20151110191048_Cachepôs.jpg', '', '', '2015-11-10 21:10:48', '2015-11-10 21:10:48'),
(25, 2, 0, '20151110191055_DSC_8915.JPG', '', '', '2015-11-10 21:11:02', '2015-11-10 21:11:02'),
(26, 2, 0, '20151110191118_comoda-verde-bambu.jpg', '', '', '2015-11-10 21:11:19', '2015-11-10 21:11:19'),
(27, 2, 0, '20151110191128_DSC_8885.JPG', '', '', '2015-11-10 21:11:35', '2015-11-10 21:11:35'),
(28, 2, 0, '20151110191151_DSC_8901.JPG', '', '', '2015-11-10 21:11:57', '2015-11-10 21:11:57'),
(29, 2, 0, '20151110191231_DSC_7742.jpg', '', '', '2015-11-10 21:12:35', '2015-11-10 21:12:35'),
(30, 2, 0, '20151110191231_DSC_7731.jpg', '', '', '2015-11-10 21:12:36', '2015-11-10 21:12:36'),
(31, 2, 0, '20151110191230_DSC_7758.jpg', '', '', '2015-11-10 21:12:36', '2015-11-10 21:12:36'),
(32, 2, 0, '20151110191230_DSC_9028.JPG', '', '', '2015-11-10 21:12:39', '2015-11-10 21:12:39'),
(33, 2, 0, '20151110191248_DSC_9023.JPG', '', '', '2015-11-10 21:12:55', '2015-11-10 21:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `marcenaria`
--

CREATE TABLE IF NOT EXISTS `marcenaria` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcenaria_categoria_imagem`
--

CREATE TABLE IF NOT EXISTS `marcenaria_categoria_imagem` (
  `id` int(10) unsigned NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  `imagem_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcenaria_imagens`
--

CREATE TABLE IF NOT EXISTS `marcenaria_imagens` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_27_150209_create_banners_table', 1),
('2015_10_27_165217_create_contato_table', 1),
('2015_10_27_165245_create_contatos_recebidos_table', 1),
('2015_10_27_170340_create_blog_categorias_table', 1),
('2015_10_27_170349_create_blog_posts_table', 1),
('2015_10_27_170358_create_blog_comentarios_table', 1),
('2015_10_27_170939_create_projetos_table', 1),
('2015_10_27_170953_create_projetos_imagens_table', 1),
('2015_10_27_171027_create_marcenaria_table', 1),
('2015_10_27_171038_create_marcenaria_imagens_table', 1),
('2015_10_27_171059_create_linhas_table', 1),
('2015_10_27_171111_create_linhas_imagens_table', 1),
('2015_10_27_171135_create_produtos_categorias_table', 1),
('2015_10_27_171145_create_produtos_table', 1),
('2015_10_27_171242_create_produtos_imagens_table', 1),
('2015_10_27_172535_create_quem_somos_table', 1),
('2015_10_27_172553_create_parceiros_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `parceiros`
--

CREATE TABLE IF NOT EXISTS `parceiros` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(10) unsigned NOT NULL,
  `produtos_categoria_id` int(10) unsigned DEFAULT NULL,
  `linha_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `linha_id`, `ordem`, `titulo`, `slug`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 0, 'Cômoda ROYAL', 'comoda-royal', 'Teste descrição.', '2015-11-10 21:18:48', '2015-11-10 21:18:48'),
(2, 4, 2, 0, 'Cômoda ROYAL', 'comoda-royal-1', 'teste descrição.', '2015-11-10 21:19:29', '2015-11-10 21:19:29'),
(3, 2, 1, 0, 'Baú AMADEIRADO', 'bau-amadeirado', 'teste descrição com \r\nmais de uma\r\nlinha', '2015-11-10 21:20:21', '2015-11-10 21:20:21'),
(4, 2, 2, 0, 'Baú Colors', 'bau-colors', 'teste\r\ndescrição\r\nmuitas\r\nlinhas\r\npara\r\ndescrição\r\ndo produto.', '2015-11-10 21:21:22', '2015-11-10 21:21:22'),
(5, 1, 2, 0, 'Nicho Colors', 'nicho-colors', 'teste', '2015-11-10 21:22:22', '2015-11-10 21:22:22'),
(6, 8, 1, 0, 'Rack Amadeirado', 'rack-amadeirado', 'teste', '2015-11-10 21:24:33', '2015-11-10 21:24:57'),
(7, 6, 1, 0, 'Cachepot Amadeirado', 'cachepot-amadeirado', 'teste', '2015-11-10 21:25:32', '2015-11-10 21:25:32'),
(8, 5, 1, 0, 'Bar Amadeirado', 'bar-amadeirado', 'teste', '2015-11-10 21:26:12', '2015-11-10 21:26:12'),
(9, 7, 1, 0, 'Carrinho Amadeirado', 'carrinho-amadeirado', 'teste', '2015-11-10 21:26:37', '2015-11-10 21:26:37'),
(11, 9, 2, 0, 'Mesa Colors', 'mesa-colors', 'teste', '2015-11-10 21:28:28', '2015-11-10 21:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `produtos_categorias`
--

CREATE TABLE IF NOT EXISTS `produtos_categorias` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 2, 'nicho volante', 'nicho-volante', '2015-11-10 21:15:02', '2015-11-10 21:17:32'),
(2, 1, 'baú', 'bau', '2015-11-10 21:15:10', '2015-11-10 21:17:26'),
(3, 8, 'estante', 'estante', '2015-11-10 21:15:15', '2015-11-10 21:17:19'),
(4, 0, 'cômoda', 'comoda', '2015-11-10 21:15:21', '2015-11-10 21:17:13'),
(5, 5, 'bar', 'bar', '2015-11-10 21:15:38', '2015-11-10 21:15:38'),
(6, 4, 'cachepot', 'cachepot', '2015-11-10 21:15:44', '2015-11-10 21:17:06'),
(7, 6, 'carrinho', 'carrinho', '2015-11-10 21:15:49', '2015-11-10 21:17:00'),
(8, 3, 'rack', 'rack', '2015-11-10 21:16:07', '2015-11-10 21:16:49'),
(9, 7, 'mesa', 'mesa', '2015-11-10 21:17:55', '2015-11-10 21:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `produtos_imagens`
--

CREATE TABLE IF NOT EXISTS `produtos_imagens` (
  `id` int(10) unsigned NOT NULL,
  `produto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `produtos_imagens`
--

INSERT INTO `produtos_imagens` (`id`, `produto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20151110191906_COMODA.jpg', '2015-11-10 21:19:07', '2015-11-10 21:19:07'),
(2, 2, 0, '20151110191948_COMODA-Vermelha-perfil-preto.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(3, 2, 0, '20151110191948_Comoda-Colors-perfilado-natural.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(4, 2, 0, '20151110191948_comoda-vermelha.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(5, 2, 0, '20151110191948_comoda-verde-bambu.jpg', '2015-11-10 21:19:49', '2015-11-10 21:19:49'),
(6, 2, 0, '20151110191948_CÔMODA-MATRIZ.jpg', '2015-11-10 21:19:50', '2015-11-10 21:19:50'),
(7, 3, 0, '20151110192047_Bau.jpeg', '2015-11-10 21:20:50', '2015-11-10 21:20:50'),
(8, 4, 0, '20151110192137_IMG_2873.JPG', '2015-11-10 21:21:37', '2015-11-10 21:21:37'),
(9, 5, 0, '20151110192237_nichos.jpg', '2015-11-10 21:22:39', '2015-11-10 21:22:39'),
(10, 5, 0, '20151110192404_nicho-quadrado-amarelo.jpg', '2015-11-10 21:24:05', '2015-11-10 21:24:05'),
(11, 5, 0, '20151110192404_nicho-quadrado-vermelho.jpg', '2015-11-10 21:24:05', '2015-11-10 21:24:05'),
(12, 6, 0, '20151110192510_rack-02.png', '2015-11-10 21:25:13', '2015-11-10 21:25:13'),
(13, 6, 0, '20151110192510_rack-01.png', '2015-11-10 21:25:13', '2015-11-10 21:25:13'),
(14, 7, 0, '20151110192549_IMG_1375.jpg', '2015-11-10 21:25:51', '2015-11-10 21:25:51'),
(15, 7, 0, '20151110192549_IMG_1434.jpg', '2015-11-10 21:25:51', '2015-11-10 21:25:51'),
(16, 8, 0, '20151110192623_bar-01.png', '2015-11-10 21:26:24', '2015-11-10 21:26:24'),
(17, 9, 0, '20151110192651_gaveteiro-.png', '2015-11-10 21:26:52', '2015-11-10 21:26:52'),
(18, 9, 0, '20151110192651_GAVETEIRO-2N.png', '2015-11-10 21:26:52', '2015-11-10 21:26:52'),
(19, 11, 0, '20151110192839_mesa-preta.jpg', '2015-11-10 21:28:41', '2015-11-10 21:28:41'),
(20, 11, 0, '20151110192839_mesa-vermelha.jpg', '2015-11-10 21:28:41', '2015-11-10 21:28:41'),
(21, 11, 0, '20151110192839_mesa-verde.jpg', '2015-11-10 21:28:41', '2015-11-10 21:28:41'),
(22, 11, 0, '20151110192839_mesa-roxa.jpg', '2015-11-10 21:28:41', '2015-11-10 21:28:41'),
(24, 5, 0, '20151110192930_Nicho-amarelo.png', '2015-11-10 21:29:32', '2015-11-10 21:29:32');

-- --------------------------------------------------------

--
-- Table structure for table `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquiteto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `titulo`, `arquiteto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, 'Cobertura Vila Olímpia', 'Arildo de Andrade Azevedo', '20151110160120_5-deck-e-portas-de-correr.jpg', '2015-11-10 18:01:23', '2015-11-10 18:01:23'),
(2, 1, 'Jardim Europa', 'Nome', '20151110160409_DORM-CASAL.jpg', '2015-11-10 18:04:09', '2015-11-10 18:04:09'),
(3, 2, 'Urimonduba', 'Nome', '20151110160623_DSC_7768_OK.jpg', '2015-11-10 18:06:27', '2015-11-10 18:06:27'),
(4, 3, 'Triconsult Brooklin', 'Nome Arquiteto', '20151110171434_10-vista.jpg', '2015-11-10 19:14:34', '2015-11-10 19:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `projetos_imagens`
--

CREATE TABLE IF NOT EXISTS `projetos_imagens` (
  `id` int(10) unsigned NOT NULL,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `legenda`, `created_at`, `updated_at`) VALUES
(1, 1, 7, '20151110160143_6-deck-spa.jpg', '', '2015-11-10 18:01:48', '2015-11-10 18:01:48'),
(2, 1, 4, '20151110160145_2-cozinha.jpg', '', '2015-11-10 18:01:49', '2015-11-10 18:01:49'),
(3, 1, 0, '20151110160150_3-sala-de-estar.jpg', '', '2015-11-10 18:01:53', '2015-11-10 18:01:53'),
(4, 1, 2, '20151110160150_4-sala-de-estar.jpg', '', '2015-11-10 18:01:54', '2015-11-10 18:01:54'),
(5, 1, 3, '20151110160153_1-cozinha.jpg', '', '2015-11-10 18:02:00', '2015-11-10 18:02:00'),
(6, 1, 6, '20151110160153_5-deck-e-portas-de-correr.jpg', '', '2015-11-10 18:02:00', '2015-11-10 18:02:00'),
(7, 1, 8, '20151110160156_7-suite-casal.jpg', '', '2015-11-10 18:02:00', '2015-11-10 18:02:00'),
(8, 1, 5, '20151110160157_8-vista-geral.jpg', '', '2015-11-10 18:02:02', '2015-11-10 18:02:02'),
(9, 1, 1, '20151110160159_9-vista-geral-II.jpg', '', '2015-11-10 18:02:05', '2015-11-10 18:02:05'),
(10, 2, 0, '20151110160420_135528_e5bbd0c75a5948568644e84f748112a5.jpg_srz_323_429_75_22_0.50_1.20_0.00_jpg_srz.jpg', '', '2015-11-10 18:04:20', '2015-11-10 18:04:20'),
(11, 2, 0, '20151110160421_COZINHA-1.jpg', '', '2015-11-10 18:04:21', '2015-11-10 18:04:21'),
(12, 2, 0, '20151110160421_DORM-CASAL.jpg', '', '2015-11-10 18:04:21', '2015-11-10 18:04:21'),
(13, 2, 0, '20151110160421_COZINHA.jpg', '', '2015-11-10 18:04:21', '2015-11-10 18:04:21'),
(14, 3, 1, '20151110164926_DSC_7864_OK.jpg', '', '2015-11-10 18:49:38', '2015-11-10 18:49:38'),
(15, 3, 0, '20151110164927_DSC_7796_OK.jpg', '', '2015-11-10 18:49:40', '2015-11-10 18:49:40'),
(16, 3, 25, '20151110164928_DSC_7773_OK.jpg', '', '2015-11-10 18:49:40', '2015-11-10 18:49:40'),
(17, 3, 2, '20151110164929_DSC_7768_OK.jpg', '', '2015-11-10 18:49:43', '2015-11-10 18:49:43'),
(18, 3, 22, '20151110164931_DSC_7784_OK.jpg', '', '2015-11-10 18:49:47', '2015-11-10 18:49:47'),
(19, 3, 11, '20151110164933_DSC_7774_OK.jpg', '', '2015-11-10 18:49:50', '2015-11-10 18:49:50'),
(20, 3, 24, '20151110164953_DSC_7875_OK.jpg', '', '2015-11-10 18:50:04', '2015-11-10 18:50:04'),
(21, 3, 23, '20151110165000_DSC_7919.jpg', '', '2015-11-10 18:50:10', '2015-11-10 18:50:10'),
(22, 3, 21, '20151110165000_DSC_7900.jpg', '', '2015-11-10 18:50:11', '2015-11-10 18:50:11'),
(23, 3, 10, '20151110165010_DSC_7942.jpg', '', '2015-11-10 18:50:21', '2015-11-10 18:50:21'),
(24, 3, 20, '20151110165011_DSC_7955.jpg', '', '2015-11-10 18:50:21', '2015-11-10 18:50:21'),
(25, 3, 19, '20151110165010_DSC_7911_OK.jpg', '', '2015-11-10 18:50:22', '2015-11-10 18:50:22'),
(26, 3, 18, '20151110165016_DSC_7959.jpg', '', '2015-11-10 18:50:27', '2015-11-10 18:50:27'),
(27, 3, 17, '20151110165022_DSC_7967.jpg', '', '2015-11-10 18:50:31', '2015-11-10 18:50:31'),
(28, 3, 16, '20151110165022_DSC_7969_OK.jpg', '', '2015-11-10 18:50:32', '2015-11-10 18:50:32'),
(29, 3, 13, '20151110165037_DSC_7990.jpg', '', '2015-11-10 18:50:45', '2015-11-10 18:50:45'),
(30, 3, 15, '20151110165040_DSC_7974.jpg', '', '2015-11-10 18:50:50', '2015-11-10 18:50:50'),
(31, 3, 14, '20151110165048_DSC_7998.jpg', '', '2015-11-10 18:51:01', '2015-11-10 18:51:01'),
(32, 3, 12, '20151110165048_DSC_7999.jpg', '', '2015-11-10 18:51:02', '2015-11-10 18:51:02'),
(33, 3, 9, '20151110165058_DSC_8016.jpg', '', '2015-11-10 18:51:11', '2015-11-10 18:51:11'),
(34, 3, 8, '20151110165059_DSC_8011_OK.jpg', '', '2015-11-10 18:51:14', '2015-11-10 18:51:14'),
(35, 3, 7, '20151110165101_DSC_8049.jpg', '', '2015-11-10 18:51:15', '2015-11-10 18:51:15'),
(36, 3, 6, '20151110165103_DSC_8051.jpg', '', '2015-11-10 18:51:19', '2015-11-10 18:51:19'),
(37, 3, 5, '20151110165112_DSC_8054.jpg', '', '2015-11-10 18:51:24', '2015-11-10 18:51:24'),
(38, 3, 4, '20151110165113_DSC_8057.jpg', '', '2015-11-10 18:51:25', '2015-11-10 18:51:25'),
(39, 3, 3, '20151110165117_DSC_8084.jpg', '', '2015-11-10 18:51:27', '2015-11-10 18:51:27'),
(40, 4, 3, '20151110171445_10-vista.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(41, 4, 2, '20151110171445_6-sala-sócios.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(42, 4, 6, '20151110171445_11-det-pia-banho.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(43, 4, 1, '20151110171445_8-estante.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(44, 4, 5, '20151110171445_12-vista-biombo.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(45, 4, 0, '20151110171445_1-staff.jpg', '', '2015-11-10 19:14:47', '2015-11-10 19:14:47'),
(46, 4, 4, '20151110171447_13-Biombo.jpg', '', '2015-11-10 19:14:49', '2015-11-10 19:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `quem_somos`
--

CREATE TABLE IF NOT EXISTS `quem_somos` (
  `id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'quem somos', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$i4QWv9joIh1EFj5Cwypb7.gMBFNk8h3iIyn.aBnT5rvQ9XOtKFZSe', 'tf0HVgG4ptOLuUe690MMRXuYdsMfG7X7Db3FdXaQl8M3cM5QIHTg6jxl6lai', '0000-00-00 00:00:00', '2015-11-09 20:29:18'),
(2, 'girante', 'girante@trupe.net', '$2y$10$7WeP5S2k2/VIaA/fGU8XA.va7g/2oZtQLiJMVew/Y1o51xdnkfUS2', NULL, '2015-11-10 17:57:36', '2015-11-10 17:57:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categorias`
--
ALTER TABLE `blog_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comentarios`
--
ALTER TABLE `blog_comentarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_comentarios_blog_post_id_foreign` (`blog_post_id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_posts_blog_categoria_id_foreign` (`blog_categoria_id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linhas`
--
ALTER TABLE `linhas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linhas_imagens`
--
ALTER TABLE `linhas_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `linhas_imagens_linha_id_foreign` (`linha_id`);

--
-- Indexes for table `marcenaria`
--
ALTER TABLE `marcenaria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marcenaria_categoria_imagem`
--
ALTER TABLE `marcenaria_categoria_imagem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marcenaria_categoria_imagem_categoria_id_index` (`categoria_id`),
  ADD KEY `marcenaria_categoria_imagem_imagem_id_index` (`imagem_id`);

--
-- Indexes for table `marcenaria_imagens`
--
ALTER TABLE `marcenaria_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parceiros`
--
ALTER TABLE `parceiros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`),
  ADD KEY `produtos_linha_id_foreign` (`linha_id`);

--
-- Indexes for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_imagens_produto_id_foreign` (`produto_id`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Indexes for table `quem_somos`
--
ALTER TABLE `quem_somos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blog_categorias`
--
ALTER TABLE `blog_categorias`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog_comentarios`
--
ALTER TABLE `blog_comentarios`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `linhas`
--
ALTER TABLE `linhas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `linhas_imagens`
--
ALTER TABLE `linhas_imagens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `marcenaria`
--
ALTER TABLE `marcenaria`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marcenaria_categoria_imagem`
--
ALTER TABLE `marcenaria_categoria_imagem`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marcenaria_imagens`
--
ALTER TABLE `marcenaria_imagens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parceiros`
--
ALTER TABLE `parceiros`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `quem_somos`
--
ALTER TABLE `quem_somos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_comentarios`
--
ALTER TABLE `blog_comentarios`
  ADD CONSTRAINT `blog_comentarios_blog_post_id_foreign` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD CONSTRAINT `blog_posts_blog_categoria_id_foreign` FOREIGN KEY (`blog_categoria_id`) REFERENCES `blog_categorias` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `linhas_imagens`
--
ALTER TABLE `linhas_imagens`
  ADD CONSTRAINT `linhas_imagens_linha_id_foreign` FOREIGN KEY (`linha_id`) REFERENCES `linhas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `marcenaria_categoria_imagem`
--
ALTER TABLE `marcenaria_categoria_imagem`
  ADD CONSTRAINT `marcenaria_categoria_imagem_imagem_id_foreign` FOREIGN KEY (`imagem_id`) REFERENCES `marcenaria_imagens` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `marcenaria_categoria_imagem_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `marcenaria` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_linha_id_foreign` FOREIGN KEY (`linha_id`) REFERENCES `linhas` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `produtos_imagens`
--
ALTER TABLE `produtos_imagens`
  ADD CONSTRAINT `produtos_imagens_produto_id_foreign` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
